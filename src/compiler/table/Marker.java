package compiler.table;

import java.util.*;

import compiler.code.*;

public class Marker {
    Marker prev;
    Label labelContinue;
    Label labelEnd;
    HashSet<String> symbols;
    Marker(Marker prev) {
        this.prev = prev;
        if (prev == null) {
            labelContinue = null;
            labelEnd = null;
        }
        else {
            labelContinue = prev.labelContinue;
            labelEnd = prev.labelEnd;
        }
        symbols = new HashSet<String>();
    }
}

