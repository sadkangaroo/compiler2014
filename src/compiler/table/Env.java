package compiler.table;

import java.util.*;

import compiler.types.*;
import compiler.errors.Errors;
import compiler.code.*;
import compiler.variable.*;

import org.antlr.v4.runtime.*;

public class Env {
    HashMap<String, Binder> table;
    HashMap<String, Label> funcLabel;
    Marker marker;
    public Env() {
        table = new HashMap<String, Binder>();
        funcLabel = new HashMap<String, Label>();
        funcLabel.put(new String("malloc"), new Label(0));
        funcLabel.put(new String("printf"), new Label(1));
        marker = null;
    }
    public boolean contains(String name) {
        return table.containsKey(new String(name));     
    }
    public Binder get(String name) {
        return table.get(new String(name));
    }
    public void put(String symbol, SpaceOffset var, ParserRuleContext ctx, HashMap<String, Record> def) {
        if (var.type() == Type.VOID) {
            Errors.reportInvalidType(ctx);
        }
        if (var.type() instanceof Record && !def.containsKey(((Record)var.type()).name())) {
            Errors.reportInvalidType(ctx);
        }
        if (marker.symbols.contains(symbol)) {
            Errors.reportRedefinition(ctx); 
        }
        var.setName(symbol);
        marker.symbols.add(symbol);
        table.put(symbol, new Binder(table.get(symbol), var)); 
    }
    public void beginScope() {
        marker = new Marker(marker);
    }
    public void endScope() {
        Iterator<String> iterator = marker.symbols.iterator();
        while (iterator.hasNext()) {
            String s = iterator.next();
            Binder binder = table.get(s);
            if (binder.last == null) {
                table.remove(s);
            }
            else {
                table.put(s, binder.last);
            }
        }
        marker = marker.prev;
    }
    public void setLoop(Label labelContinue, Label labelEnd) {
        marker.labelContinue = labelContinue;
        marker.labelEnd = labelEnd;
    }
    public Label labelContinue() {
        return marker.labelContinue;
    }
    public Label labelEnd() {
        return marker.labelEnd; 
    }
    public void setFunction(Type returnType, String name, Label label) {
        funcLabel.put(new String(name), label);
    }
    public Label getLabel(String name) {
        return funcLabel.get(new String(name));
    }
}
