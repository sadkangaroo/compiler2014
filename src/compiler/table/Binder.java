package compiler.table;

import compiler.types.*;
import compiler.variable.*;

public class Binder {
    Binder last;
    SpaceOffset var;
    Binder(Binder last, SpaceOffset var) {
        this.last = last;
        this.var = var;
    }
    public Type type() {
        return var.type();
    }
    public Variable var() {
        return var;
    }
}

