package compiler.variable;

import compiler.types.*;

public final class Register extends Variable {
    public static Register RA = new Register("$ra");
    public static Register A0 = new Register("$a0");
    public Register(Type type, String name) {
        this.type = type;
        this.name = name;
    }
    public Register(String name) {
        this.type = new Int();
        this.name = name;
    }
}
