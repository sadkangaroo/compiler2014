package compiler.variable;

import compiler.types.*;

public final class SpaceVar extends Storeable {
    Variable indicator;
    int offset;
    public SpaceVar(Type type, SpaceOffset indicator, int offset) {
        this.type = type;
        this.indicator = indicator;
        this.offset = offset;
    }
    public SpaceVar displace(Type type, int delta) {
        return new SpaceVar(type, (SpaceOffset)indicator, offset + delta);
    }
    public Variable indicator() {
        return indicator;
    }
    public int offset() {
        return offset;
    }
}
