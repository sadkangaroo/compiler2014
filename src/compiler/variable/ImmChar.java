package compiler.variable;

import compiler.types.*;

public final class ImmChar extends Immediate {
    String ch;
    public ImmChar(String ch) {
        this.ch = ch;
        type = new Char();
    }
    public String ch() {
        return ch;
    }
}
