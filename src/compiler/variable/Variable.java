package compiler.variable;

import compiler.types.*;
import compiler.optimizer.*;

public abstract class Variable {
    String name;
    Type type;
    public Type type() {
        return type;
    }
    public String name() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setType(Type type) {
        this.type = type;
    }
    public int id() {
        return ((SpaceOffset)this).id;
    }
    public boolean localRegisble() {
        return this instanceof SpaceOffset && !((SpaceOffset)this).isGlobal && ((SpaceOffset)this).regisble;
    }
    public Variable allocate() {
        if (this instanceof SpaceOffset) {
            SpaceOffset var = (SpaceOffset)this;
            if (!var.isGlobal && Analyzer.regs[var.id] != null) {
                return new Register(type, Analyzer.regs[var.id]);
            }
            if (var.isGlobal && var.regisble) {
                if (Analyzer.globalReg.containsKey(var)) {
                    return Analyzer.globalReg.get(var);
                }
                else if (!Analyzer.remains.isEmpty()) {
                    String cnt = Analyzer.remains.last();
                    Analyzer.remains.remove(Analyzer.remains.last());
                    Analyzer.globalReg.put(var, new Register(new Int(), cnt));
                    return Analyzer.globalReg.get(var);
                }
            }
        }
        else if (this instanceof SpaceVar) {
            SpaceVar var = (SpaceVar)this;
            var.indicator = var.indicator().allocate();
        }
        return this;
    }
    public String fall(String back) {
        if (this instanceof Register) {
            return ((Register)this).name();
        }
        else {
            return back;
        }
    }
}
