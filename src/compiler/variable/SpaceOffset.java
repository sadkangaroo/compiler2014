package compiler.variable;

import compiler.types.*;

import java.util.*;

public final class SpaceOffset extends Storeable{
    int id;
    int offset;
    boolean isGlobal;
    public String str;
    public boolean regisble;
    public static ArrayList<SpaceOffset> localVar = new ArrayList<SpaceOffset>();
    public static ArrayList<SpaceOffset> globalVar = new ArrayList<SpaceOffset>();
    public static int spId = 0, gpId = 0;
    public static int gpCnt = -32768 + 24, spCnt = 0;
    public SpaceOffset(Type type, boolean isGlobal) {
        this.type = type;
        this.isGlobal = isGlobal;
        regisble = type instanceof Arithmetic;
        str = null;
        if (isGlobal) {
            id = gpId++;
            globalVar.add(this);
            offset = gpCnt;
            gpCnt += type.size();
        }
        else {
            id = spId++;
            localVar.add(this);
            spCnt -= type.size();
            offset = spCnt;
        }
    }
    public SpaceOffset(Type type, boolean isGlobal, int offset) {
        this.type = type;
        this.isGlobal = isGlobal;
        this.offset = offset;
        str = null;
        if (isGlobal) {
            id = gpId++;
            globalVar.add(this);
        }
        else {
            id = spId++;
            localVar.add(this);
        }
    }
    public SpaceOffset displace(Type type, int delta) {
        return new SpaceOffset(type, isGlobal, offset + delta);
    }
    public boolean isGlobal() {
        return isGlobal;
    }
    public int offset() {
        return offset;
    }
    @Override
    public String toString() {
        if (isGlobal) {
            return offset + "($gp)";
        }
        else {
            return offset + "($sp)";
        }
    }
}
