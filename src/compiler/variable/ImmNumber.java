package compiler.variable;

import compiler.types.*;

public final class ImmNumber extends Immediate {
    int num;
    public ImmNumber(int num) {
        this.num = num;
        type = new Int();
    }
    public int num() {
        return num;
    }
}
