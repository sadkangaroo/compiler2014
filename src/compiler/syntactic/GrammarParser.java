// Generated from Grammar.g4 by ANTLR 4.2
package compiler.syntactic;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GrammarParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__56=1, T__55=2, T__54=3, T__53=4, T__52=5, T__51=6, T__50=7, T__49=8, 
		T__48=9, T__47=10, T__46=11, T__45=12, T__44=13, T__43=14, T__42=15, T__41=16, 
		T__40=17, T__39=18, T__38=19, T__37=20, T__36=21, T__35=22, T__34=23, 
		T__33=24, T__32=25, T__31=26, T__30=27, T__29=28, T__28=29, T__27=30, 
		T__26=31, T__25=32, T__24=33, T__23=34, T__22=35, T__21=36, T__20=37, 
		T__19=38, T__18=39, T__17=40, T__16=41, T__15=42, T__14=43, T__13=44, 
		T__12=45, T__11=46, T__10=47, T__9=48, T__8=49, T__7=50, T__6=51, T__5=52, 
		T__4=53, T__3=54, T__2=55, T__1=56, T__0=57, Identifier=58, DecimalConstant=59, 
		OctalConstant=60, HexadecimalConstant=61, CharacterConstant=62, StringLiteral=63, 
		Whitespace=64, Newline=65, BlockComment=66, LineComment=67, IncludeLine=68;
	public static final String[] tokenNames = {
		"<INVALID>", "'&'", "'['", "'*'", "'<'", "'--'", "'continue'", "'!='", 
		"'<='", "'<<'", "'}'", "'char'", "'%'", "'->'", "'union'", "'*='", "')'", 
		"'='", "'|='", "'|'", "'!'", "'sizeof'", "'<<='", "']'", "'-='", "','", 
		"'-'", "'while'", "'('", "'&='", "'if'", "'int'", "'void'", "'>>='", "'{'", 
		"'break'", "'+='", "'^='", "'else'", "'struct'", "'++'", "'>>'", "'^'", 
		"'.'", "'+'", "'8 + 8 - 1'", "'for'", "'return'", "';'", "'&&'", "'||'", 
		"'>'", "'%='", "'/='", "'=='", "'/'", "'~'", "'>='", "Identifier", "DecimalConstant", 
		"OctalConstant", "HexadecimalConstant", "CharacterConstant", "StringLiteral", 
		"Whitespace", "Newline", "BlockComment", "LineComment", "IncludeLine"
	};
	public static final int
		RULE_compilationUnit = 0, RULE_program = 1, RULE_declaration = 2, RULE_functionDefinition = 3, 
		RULE_initDeclaratorList = 4, RULE_initDeclarator = 5, RULE_initializer = 6, 
		RULE_declarator = 7, RULE_arrayDeclarator = 8, RULE_arrayCapacity = 9, 
		RULE_parameterList = 10, RULE_parameterDeclaration = 11, RULE_typeSpecifier = 12, 
		RULE_declaratorList = 13, RULE_statement = 14, RULE_expressionStatement = 15, 
		RULE_compoundStatement = 16, RULE_selectionStatement = 17, RULE_whileStatement = 18, 
		RULE_forStatement = 19, RULE_expression1 = 20, RULE_expression2 = 21, 
		RULE_expression3 = 22, RULE_jumpStatement = 23, RULE_expression = 24, 
		RULE_assignmentExpression = 25, RULE_arithmeticExpression = 26, RULE_castExpression = 27, 
		RULE_unaryExpression = 28, RULE_typeName = 29, RULE_postfixExpression = 30, 
		RULE_arguments = 31, RULE_primaryExpression = 32, RULE_integerConstant = 33;
	public static final String[] ruleNames = {
		"compilationUnit", "program", "declaration", "functionDefinition", "initDeclaratorList", 
		"initDeclarator", "initializer", "declarator", "arrayDeclarator", "arrayCapacity", 
		"parameterList", "parameterDeclaration", "typeSpecifier", "declaratorList", 
		"statement", "expressionStatement", "compoundStatement", "selectionStatement", 
		"whileStatement", "forStatement", "expression1", "expression2", "expression3", 
		"jumpStatement", "expression", "assignmentExpression", "arithmeticExpression", 
		"castExpression", "unaryExpression", "typeName", "postfixExpression", 
		"arguments", "primaryExpression", "integerConstant"
	};

	@Override
	public String getGrammarFileName() { return "Grammar.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public GrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class CompilationUnitContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(GrammarParser.EOF, 0); }
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public CompilationUnitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compilationUnit; }
	}

	public final CompilationUnitContext compilationUnit() throws RecognitionException {
		CompilationUnitContext _localctx = new CompilationUnitContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_compilationUnit);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68); program();
			setState(69); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public FunctionDefinitionContext functionDefinition(int i) {
			return getRuleContext(FunctionDefinitionContext.class,i);
		}
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public List<FunctionDefinitionContext> functionDefinition() {
			return getRuleContexts(FunctionDefinitionContext.class);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 11) | (1L << 14) | (1L << 31) | (1L << 32) | (1L << 39) | (1L << 48))) != 0)) {
				{
				setState(74);
				switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
				case 1:
					{
					setState(71); declaration();
					}
					break;

				case 2:
					{
					setState(72); functionDefinition();
					}
					break;

				case 3:
					{
					setState(73); match(48);
					}
					break;
				}
				}
				setState(78);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public InitDeclaratorListContext initDeclaratorList() {
			return getRuleContext(InitDeclaratorListContext.class,0);
		}
		public TypeSpecifierContext typeSpecifier() {
			return getRuleContext(TypeSpecifierContext.class,0);
		}
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(79); typeSpecifier();
			setState(81);
			_la = _input.LA(1);
			if (_la==3 || _la==Identifier) {
				{
				setState(80); initDeclaratorList();
				}
			}

			setState(83); match(48);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDefinitionContext extends ParserRuleContext {
		public TypeSpecifierContext typeSpecifier() {
			return getRuleContext(TypeSpecifierContext.class,0);
		}
		public DeclaratorContext declarator() {
			return getRuleContext(DeclaratorContext.class,0);
		}
		public CompoundStatementContext compoundStatement() {
			return getRuleContext(CompoundStatementContext.class,0);
		}
		public FunctionDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDefinition; }
	}

	public final FunctionDefinitionContext functionDefinition() throws RecognitionException {
		FunctionDefinitionContext _localctx = new FunctionDefinitionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_functionDefinition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(85); typeSpecifier();
			setState(86); declarator();
			setState(87); compoundStatement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitDeclaratorListContext extends ParserRuleContext {
		public List<InitDeclaratorContext> initDeclarator() {
			return getRuleContexts(InitDeclaratorContext.class);
		}
		public InitDeclaratorContext initDeclarator(int i) {
			return getRuleContext(InitDeclaratorContext.class,i);
		}
		public InitDeclaratorListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initDeclaratorList; }
	}

	public final InitDeclaratorListContext initDeclaratorList() throws RecognitionException {
		InitDeclaratorListContext _localctx = new InitDeclaratorListContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_initDeclaratorList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89); initDeclarator();
			setState(94);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==25) {
				{
				{
				setState(90); match(25);
				setState(91); initDeclarator();
				}
				}
				setState(96);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitDeclaratorContext extends ParserRuleContext {
		public InitializerContext initializer() {
			return getRuleContext(InitializerContext.class,0);
		}
		public DeclaratorContext declarator() {
			return getRuleContext(DeclaratorContext.class,0);
		}
		public InitDeclaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initDeclarator; }
	}

	public final InitDeclaratorContext initDeclarator() throws RecognitionException {
		InitDeclaratorContext _localctx = new InitDeclaratorContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_initDeclarator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97); declarator();
			setState(100);
			_la = _input.LA(1);
			if (_la==17) {
				{
				setState(98); match(17);
				setState(99); initializer();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitializerContext extends ParserRuleContext {
		public AssignmentExpressionContext assignmentExpression() {
			return getRuleContext(AssignmentExpressionContext.class,0);
		}
		public InitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initializer; }
	}

	public final InitializerContext initializer() throws RecognitionException {
		InitializerContext _localctx = new InitializerContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_initializer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(102); assignmentExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclaratorContext extends ParserRuleContext {
		public ArrayDeclaratorContext arrayDeclarator() {
			return getRuleContext(ArrayDeclaratorContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(GrammarParser.Identifier, 0); }
		public DeclaratorContext declarator() {
			return getRuleContext(DeclaratorContext.class,0);
		}
		public ParameterListContext parameterList() {
			return getRuleContext(ParameterListContext.class,0);
		}
		public DeclaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declarator; }
	}

	public final DeclaratorContext declarator() throws RecognitionException {
		DeclaratorContext _localctx = new DeclaratorContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_declarator);
		int _la;
		try {
			setState(113);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(104); match(Identifier);
				setState(105); match(28);
				setState(107);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 11) | (1L << 14) | (1L << 31) | (1L << 32) | (1L << 39))) != 0)) {
					{
					setState(106); parameterList();
					}
				}

				setState(109); match(16);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(110); match(3);
				setState(111); declarator();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(112); arrayDeclarator(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayDeclaratorContext extends ParserRuleContext {
		public ArrayDeclaratorContext arrayDeclarator() {
			return getRuleContext(ArrayDeclaratorContext.class,0);
		}
		public ArrayCapacityContext arrayCapacity() {
			return getRuleContext(ArrayCapacityContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(GrammarParser.Identifier, 0); }
		public ArrayDeclaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayDeclarator; }
	}

	public final ArrayDeclaratorContext arrayDeclarator() throws RecognitionException {
		return arrayDeclarator(0);
	}

	private ArrayDeclaratorContext arrayDeclarator(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ArrayDeclaratorContext _localctx = new ArrayDeclaratorContext(_ctx, _parentState);
		ArrayDeclaratorContext _prevctx = _localctx;
		int _startState = 16;
		enterRecursionRule(_localctx, 16, RULE_arrayDeclarator, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(116); match(Identifier);
			}
			_ctx.stop = _input.LT(-1);
			setState(125);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ArrayDeclaratorContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_arrayDeclarator);
					setState(118);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(119); match(2);
					setState(120); arrayCapacity();
					setState(121); match(23);
					}
					} 
				}
				setState(127);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ArrayCapacityContext extends ParserRuleContext {
		public IntegerConstantContext integerConstant() {
			return getRuleContext(IntegerConstantContext.class,0);
		}
		public ArrayCapacityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayCapacity; }
	}

	public final ArrayCapacityContext arrayCapacity() throws RecognitionException {
		ArrayCapacityContext _localctx = new ArrayCapacityContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_arrayCapacity);
		try {
			setState(130);
			switch (_input.LA(1)) {
			case DecimalConstant:
			case OctalConstant:
			case HexadecimalConstant:
				enterOuterAlt(_localctx, 1);
				{
				setState(128); integerConstant();
				}
				break;
			case 45:
				enterOuterAlt(_localctx, 2);
				{
				setState(129); match(45);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterListContext extends ParserRuleContext {
		public ParameterDeclarationContext parameterDeclaration(int i) {
			return getRuleContext(ParameterDeclarationContext.class,i);
		}
		public List<ParameterDeclarationContext> parameterDeclaration() {
			return getRuleContexts(ParameterDeclarationContext.class);
		}
		public ParameterListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameterList; }
	}

	public final ParameterListContext parameterList() throws RecognitionException {
		ParameterListContext _localctx = new ParameterListContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_parameterList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(132); parameterDeclaration();
			setState(137);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==25) {
				{
				{
				setState(133); match(25);
				setState(134); parameterDeclaration();
				}
				}
				setState(139);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterDeclarationContext extends ParserRuleContext {
		public TypeSpecifierContext typeSpecifier() {
			return getRuleContext(TypeSpecifierContext.class,0);
		}
		public DeclaratorContext declarator() {
			return getRuleContext(DeclaratorContext.class,0);
		}
		public ParameterDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameterDeclaration; }
	}

	public final ParameterDeclarationContext parameterDeclaration() throws RecognitionException {
		ParameterDeclarationContext _localctx = new ParameterDeclarationContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_parameterDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140); typeSpecifier();
			setState(141); declarator();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeSpecifierContext extends ParserRuleContext {
		public List<TypeSpecifierContext> typeSpecifier() {
			return getRuleContexts(TypeSpecifierContext.class);
		}
		public TerminalNode Identifier() { return getToken(GrammarParser.Identifier, 0); }
		public List<DeclaratorListContext> declaratorList() {
			return getRuleContexts(DeclaratorListContext.class);
		}
		public DeclaratorListContext declaratorList(int i) {
			return getRuleContext(DeclaratorListContext.class,i);
		}
		public TypeSpecifierContext typeSpecifier(int i) {
			return getRuleContext(TypeSpecifierContext.class,i);
		}
		public TypeSpecifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeSpecifier; }
	}

	public final TypeSpecifierContext typeSpecifier() throws RecognitionException {
		TypeSpecifierContext _localctx = new TypeSpecifierContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_typeSpecifier);
		int _la;
		try {
			setState(163);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(143); match(32);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(144); match(11);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(145); match(31);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(146);
				_la = _input.LA(1);
				if ( !(_la==14 || _la==39) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(148);
				_la = _input.LA(1);
				if (_la==Identifier) {
					{
					setState(147); match(Identifier);
					}
				}

				setState(150); match(34);
				setState(155); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(151); typeSpecifier();
					setState(152); declaratorList();
					setState(153); match(48);
					}
					}
					setState(157); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 11) | (1L << 14) | (1L << 31) | (1L << 32) | (1L << 39))) != 0) );
				setState(159); match(10);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(161);
				_la = _input.LA(1);
				if ( !(_la==14 || _la==39) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(162); match(Identifier);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclaratorListContext extends ParserRuleContext {
		public DeclaratorContext declarator(int i) {
			return getRuleContext(DeclaratorContext.class,i);
		}
		public List<DeclaratorContext> declarator() {
			return getRuleContexts(DeclaratorContext.class);
		}
		public DeclaratorListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaratorList; }
	}

	public final DeclaratorListContext declaratorList() throws RecognitionException {
		DeclaratorListContext _localctx = new DeclaratorListContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_declaratorList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(165); declarator();
			setState(170);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==25) {
				{
				{
				setState(166); match(25);
				setState(167); declarator();
				}
				}
				setState(172);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public JumpStatementContext jumpStatement() {
			return getRuleContext(JumpStatementContext.class,0);
		}
		public SelectionStatementContext selectionStatement() {
			return getRuleContext(SelectionStatementContext.class,0);
		}
		public WhileStatementContext whileStatement() {
			return getRuleContext(WhileStatementContext.class,0);
		}
		public ForStatementContext forStatement() {
			return getRuleContext(ForStatementContext.class,0);
		}
		public CompoundStatementContext compoundStatement() {
			return getRuleContext(CompoundStatementContext.class,0);
		}
		public ExpressionStatementContext expressionStatement() {
			return getRuleContext(ExpressionStatementContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_statement);
		try {
			setState(179);
			switch (_input.LA(1)) {
			case 1:
			case 3:
			case 5:
			case 20:
			case 21:
			case 26:
			case 28:
			case 40:
			case 44:
			case 48:
			case 56:
			case Identifier:
			case DecimalConstant:
			case OctalConstant:
			case HexadecimalConstant:
			case CharacterConstant:
			case StringLiteral:
				enterOuterAlt(_localctx, 1);
				{
				setState(173); expressionStatement();
				}
				break;
			case 34:
				enterOuterAlt(_localctx, 2);
				{
				setState(174); compoundStatement();
				}
				break;
			case 30:
				enterOuterAlt(_localctx, 3);
				{
				setState(175); selectionStatement();
				}
				break;
			case 27:
				enterOuterAlt(_localctx, 4);
				{
				setState(176); whileStatement();
				}
				break;
			case 46:
				enterOuterAlt(_localctx, 5);
				{
				setState(177); forStatement();
				}
				break;
			case 6:
			case 35:
			case 47:
				enterOuterAlt(_localctx, 6);
				{
				setState(178); jumpStatement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionStatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExpressionStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionStatement; }
	}

	public final ExpressionStatementContext expressionStatement() throws RecognitionException {
		ExpressionStatementContext _localctx = new ExpressionStatementContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_expressionStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(182);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 28) | (1L << 40) | (1L << 44) | (1L << 56) | (1L << Identifier) | (1L << DecimalConstant) | (1L << OctalConstant) | (1L << HexadecimalConstant) | (1L << CharacterConstant) | (1L << StringLiteral))) != 0)) {
				{
				setState(181); expression();
				}
			}

			setState(184); match(48);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompoundStatementContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public CompoundStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compoundStatement; }
	}

	public final CompoundStatementContext compoundStatement() throws RecognitionException {
		CompoundStatementContext _localctx = new CompoundStatementContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_compoundStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(186); match(34);
			setState(191);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 6) | (1L << 11) | (1L << 14) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 27) | (1L << 28) | (1L << 30) | (1L << 31) | (1L << 32) | (1L << 34) | (1L << 35) | (1L << 39) | (1L << 40) | (1L << 44) | (1L << 46) | (1L << 47) | (1L << 48) | (1L << 56) | (1L << Identifier) | (1L << DecimalConstant) | (1L << OctalConstant) | (1L << HexadecimalConstant) | (1L << CharacterConstant) | (1L << StringLiteral))) != 0)) {
				{
				setState(189);
				switch (_input.LA(1)) {
				case 11:
				case 14:
				case 31:
				case 32:
				case 39:
					{
					setState(187); declaration();
					}
					break;
				case 1:
				case 3:
				case 5:
				case 6:
				case 20:
				case 21:
				case 26:
				case 27:
				case 28:
				case 30:
				case 34:
				case 35:
				case 40:
				case 44:
				case 46:
				case 47:
				case 48:
				case 56:
				case Identifier:
				case DecimalConstant:
				case OctalConstant:
				case HexadecimalConstant:
				case CharacterConstant:
				case StringLiteral:
					{
					setState(188); statement();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(193);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(194); match(10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectionStatementContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public SelectionStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectionStatement; }
	}

	public final SelectionStatementContext selectionStatement() throws RecognitionException {
		SelectionStatementContext _localctx = new SelectionStatementContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_selectionStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(196); match(30);
			setState(197); match(28);
			setState(198); expression();
			setState(199); match(16);
			setState(200); statement();
			setState(203);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				{
				setState(201); match(38);
				setState(202); statement();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileStatementContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public WhileStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileStatement; }
	}

	public final WhileStatementContext whileStatement() throws RecognitionException {
		WhileStatementContext _localctx = new WhileStatementContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_whileStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(205); match(27);
			setState(206); match(28);
			setState(207); expression();
			setState(208); match(16);
			setState(209); statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForStatementContext extends ParserRuleContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public Expression3Context expression3() {
			return getRuleContext(Expression3Context.class,0);
		}
		public Expression2Context expression2() {
			return getRuleContext(Expression2Context.class,0);
		}
		public Expression1Context expression1() {
			return getRuleContext(Expression1Context.class,0);
		}
		public ForStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forStatement; }
	}

	public final ForStatementContext forStatement() throws RecognitionException {
		ForStatementContext _localctx = new ForStatementContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_forStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(211); match(46);
			setState(212); match(28);
			setState(214);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 28) | (1L << 40) | (1L << 44) | (1L << 56) | (1L << Identifier) | (1L << DecimalConstant) | (1L << OctalConstant) | (1L << HexadecimalConstant) | (1L << CharacterConstant) | (1L << StringLiteral))) != 0)) {
				{
				setState(213); expression1();
				}
			}

			setState(216); match(48);
			setState(218);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 28) | (1L << 40) | (1L << 44) | (1L << 56) | (1L << Identifier) | (1L << DecimalConstant) | (1L << OctalConstant) | (1L << HexadecimalConstant) | (1L << CharacterConstant) | (1L << StringLiteral))) != 0)) {
				{
				setState(217); expression2();
				}
			}

			setState(220); match(48);
			setState(222);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 28) | (1L << 40) | (1L << 44) | (1L << 56) | (1L << Identifier) | (1L << DecimalConstant) | (1L << OctalConstant) | (1L << HexadecimalConstant) | (1L << CharacterConstant) | (1L << StringLiteral))) != 0)) {
				{
				setState(221); expression3();
				}
			}

			setState(224); match(16);
			setState(225); statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expression1Context extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Expression1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression1; }
	}

	public final Expression1Context expression1() throws RecognitionException {
		Expression1Context _localctx = new Expression1Context(_ctx, getState());
		enterRule(_localctx, 40, RULE_expression1);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(227); expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expression2Context extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Expression2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression2; }
	}

	public final Expression2Context expression2() throws RecognitionException {
		Expression2Context _localctx = new Expression2Context(_ctx, getState());
		enterRule(_localctx, 42, RULE_expression2);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(229); expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expression3Context extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Expression3Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression3; }
	}

	public final Expression3Context expression3() throws RecognitionException {
		Expression3Context _localctx = new Expression3Context(_ctx, getState());
		enterRule(_localctx, 44, RULE_expression3);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(231); expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JumpStatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public JumpStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jumpStatement; }
	}

	public final JumpStatementContext jumpStatement() throws RecognitionException {
		JumpStatementContext _localctx = new JumpStatementContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_jumpStatement);
		int _la;
		try {
			setState(242);
			switch (_input.LA(1)) {
			case 6:
				enterOuterAlt(_localctx, 1);
				{
				setState(233); match(6);
				setState(234); match(48);
				}
				break;
			case 35:
				enterOuterAlt(_localctx, 2);
				{
				setState(235); match(35);
				setState(236); match(48);
				}
				break;
			case 47:
				enterOuterAlt(_localctx, 3);
				{
				setState(237); match(47);
				setState(239);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 28) | (1L << 40) | (1L << 44) | (1L << 56) | (1L << Identifier) | (1L << DecimalConstant) | (1L << OctalConstant) | (1L << HexadecimalConstant) | (1L << CharacterConstant) | (1L << StringLiteral))) != 0)) {
					{
					setState(238); expression();
					}
				}

				setState(241); match(48);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public AssignmentExpressionContext assignmentExpression(int i) {
			return getRuleContext(AssignmentExpressionContext.class,i);
		}
		public List<AssignmentExpressionContext> assignmentExpression() {
			return getRuleContexts(AssignmentExpressionContext.class);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(244); assignmentExpression();
			setState(249);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==25) {
				{
				{
				setState(245); match(25);
				setState(246); assignmentExpression();
				}
				}
				setState(251);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentExpressionContext extends ParserRuleContext {
		public ArithmeticExpressionContext arithmeticExpression() {
			return getRuleContext(ArithmeticExpressionContext.class,0);
		}
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public AssignmentExpressionContext assignmentExpression() {
			return getRuleContext(AssignmentExpressionContext.class,0);
		}
		public AssignmentExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentExpression; }
	}

	public final AssignmentExpressionContext assignmentExpression() throws RecognitionException {
		AssignmentExpressionContext _localctx = new AssignmentExpressionContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_assignmentExpression);
		int _la;
		try {
			setState(257);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(252); arithmeticExpression(0);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(253); unaryExpression();
				setState(254);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 15) | (1L << 17) | (1L << 18) | (1L << 22) | (1L << 24) | (1L << 29) | (1L << 33) | (1L << 36) | (1L << 37) | (1L << 52) | (1L << 53))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(255); assignmentExpression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArithmeticExpressionContext extends ParserRuleContext {
		public List<ArithmeticExpressionContext> arithmeticExpression() {
			return getRuleContexts(ArithmeticExpressionContext.class);
		}
		public CastExpressionContext castExpression() {
			return getRuleContext(CastExpressionContext.class,0);
		}
		public ArithmeticExpressionContext arithmeticExpression(int i) {
			return getRuleContext(ArithmeticExpressionContext.class,i);
		}
		public ArithmeticExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmeticExpression; }
	}

	public final ArithmeticExpressionContext arithmeticExpression() throws RecognitionException {
		return arithmeticExpression(0);
	}

	private ArithmeticExpressionContext arithmeticExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ArithmeticExpressionContext _localctx = new ArithmeticExpressionContext(_ctx, _parentState);
		ArithmeticExpressionContext _prevctx = _localctx;
		int _startState = 52;
		enterRecursionRule(_localctx, 52, RULE_arithmeticExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(260); castExpression();
			}
			_ctx.stop = _input.LT(-1);
			setState(294);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(292);
					switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
					case 1:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(262);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(263);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 3) | (1L << 12) | (1L << 55))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(264); arithmeticExpression(11);
						}
						break;

					case 2:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(265);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(266);
						_la = _input.LA(1);
						if ( !(_la==26 || _la==44) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(267); arithmeticExpression(10);
						}
						break;

					case 3:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(268);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(269);
						_la = _input.LA(1);
						if ( !(_la==9 || _la==41) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(270); arithmeticExpression(9);
						}
						break;

					case 4:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(271);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(272);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 4) | (1L << 8) | (1L << 51) | (1L << 57))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(273); arithmeticExpression(8);
						}
						break;

					case 5:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(274);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(275);
						_la = _input.LA(1);
						if ( !(_la==7 || _la==54) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(276); arithmeticExpression(7);
						}
						break;

					case 6:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(277);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(278); match(1);
						setState(279); arithmeticExpression(6);
						}
						break;

					case 7:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(280);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(281); match(42);
						setState(282); arithmeticExpression(5);
						}
						break;

					case 8:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(283);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(284); match(19);
						setState(285); arithmeticExpression(4);
						}
						break;

					case 9:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(286);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(287); match(49);
						setState(288); arithmeticExpression(3);
						}
						break;

					case 10:
						{
						_localctx = new ArithmeticExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arithmeticExpression);
						setState(289);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(290); match(50);
						setState(291); arithmeticExpression(2);
						}
						break;
					}
					} 
				}
				setState(296);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class CastExpressionContext extends ParserRuleContext {
		public CastExpressionContext castExpression() {
			return getRuleContext(CastExpressionContext.class,0);
		}
		public TypeNameContext typeName() {
			return getRuleContext(TypeNameContext.class,0);
		}
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public CastExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_castExpression; }
	}

	public final CastExpressionContext castExpression() throws RecognitionException {
		CastExpressionContext _localctx = new CastExpressionContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_castExpression);
		try {
			setState(303);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(297); unaryExpression();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(298); match(28);
				setState(299); typeName(0);
				setState(300); match(16);
				setState(301); castExpression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryExpressionContext extends ParserRuleContext {
		public CastExpressionContext castExpression() {
			return getRuleContext(CastExpressionContext.class,0);
		}
		public TypeNameContext typeName() {
			return getRuleContext(TypeNameContext.class,0);
		}
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public PostfixExpressionContext postfixExpression() {
			return getRuleContext(PostfixExpressionContext.class,0);
		}
		public UnaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryExpression; }
	}

	public final UnaryExpressionContext unaryExpression() throws RecognitionException {
		UnaryExpressionContext _localctx = new UnaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_unaryExpression);
		int _la;
		try {
			setState(318);
			switch (_input.LA(1)) {
			case 28:
			case Identifier:
			case DecimalConstant:
			case OctalConstant:
			case HexadecimalConstant:
			case CharacterConstant:
			case StringLiteral:
				enterOuterAlt(_localctx, 1);
				{
				setState(305); postfixExpression(0);
				}
				break;
			case 5:
			case 40:
				enterOuterAlt(_localctx, 2);
				{
				setState(306);
				_la = _input.LA(1);
				if ( !(_la==5 || _la==40) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(307); unaryExpression();
				}
				break;
			case 1:
			case 3:
			case 20:
			case 26:
			case 44:
			case 56:
				enterOuterAlt(_localctx, 3);
				{
				setState(308);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 20) | (1L << 26) | (1L << 44) | (1L << 56))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(309); castExpression();
				}
				break;
			case 21:
				enterOuterAlt(_localctx, 4);
				{
				setState(310); match(21);
				setState(316);
				switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
				case 1:
					{
					setState(311); unaryExpression();
					}
					break;

				case 2:
					{
					setState(312); match(28);
					setState(313); typeName(0);
					setState(314); match(16);
					}
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeNameContext extends ParserRuleContext {
		public TypeNameContext typeName() {
			return getRuleContext(TypeNameContext.class,0);
		}
		public TypeSpecifierContext typeSpecifier() {
			return getRuleContext(TypeSpecifierContext.class,0);
		}
		public TypeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeName; }
	}

	public final TypeNameContext typeName() throws RecognitionException {
		return typeName(0);
	}

	private TypeNameContext typeName(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TypeNameContext _localctx = new TypeNameContext(_ctx, _parentState);
		TypeNameContext _prevctx = _localctx;
		int _startState = 58;
		enterRecursionRule(_localctx, 58, RULE_typeName, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(321); typeSpecifier();
			}
			_ctx.stop = _input.LT(-1);
			setState(327);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new TypeNameContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_typeName);
					setState(323);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(324); match(3);
					}
					} 
				}
				setState(329);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PostfixExpressionContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(GrammarParser.Identifier, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ArgumentsContext arguments() {
			return getRuleContext(ArgumentsContext.class,0);
		}
		public PrimaryExpressionContext primaryExpression() {
			return getRuleContext(PrimaryExpressionContext.class,0);
		}
		public PostfixExpressionContext postfixExpression() {
			return getRuleContext(PostfixExpressionContext.class,0);
		}
		public PostfixExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfixExpression; }
	}

	public final PostfixExpressionContext postfixExpression() throws RecognitionException {
		return postfixExpression(0);
	}

	private PostfixExpressionContext postfixExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PostfixExpressionContext _localctx = new PostfixExpressionContext(_ctx, _parentState);
		PostfixExpressionContext _prevctx = _localctx;
		int _startState = 60;
		enterRecursionRule(_localctx, 60, RULE_postfixExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(331); primaryExpression();
			}
			_ctx.stop = _input.LT(-1);
			setState(356);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,34,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(354);
					switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
					case 1:
						{
						_localctx = new PostfixExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_postfixExpression);
						setState(333);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(334); match(2);
						setState(335); expression();
						setState(336); match(23);
						}
						break;

					case 2:
						{
						_localctx = new PostfixExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_postfixExpression);
						setState(338);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(339); match(28);
						setState(341);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 3) | (1L << 5) | (1L << 20) | (1L << 21) | (1L << 26) | (1L << 28) | (1L << 40) | (1L << 44) | (1L << 56) | (1L << Identifier) | (1L << DecimalConstant) | (1L << OctalConstant) | (1L << HexadecimalConstant) | (1L << CharacterConstant) | (1L << StringLiteral))) != 0)) {
							{
							setState(340); arguments();
							}
						}

						setState(343); match(16);
						}
						break;

					case 3:
						{
						_localctx = new PostfixExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_postfixExpression);
						setState(344);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(345); match(43);
						setState(346); match(Identifier);
						}
						break;

					case 4:
						{
						_localctx = new PostfixExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_postfixExpression);
						setState(347);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(348); match(13);
						setState(349); match(Identifier);
						}
						break;

					case 5:
						{
						_localctx = new PostfixExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_postfixExpression);
						setState(350);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(351); match(40);
						}
						break;

					case 6:
						{
						_localctx = new PostfixExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_postfixExpression);
						setState(352);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(353); match(5);
						}
						break;
					}
					} 
				}
				setState(358);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,34,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ArgumentsContext extends ParserRuleContext {
		public AssignmentExpressionContext assignmentExpression(int i) {
			return getRuleContext(AssignmentExpressionContext.class,i);
		}
		public List<AssignmentExpressionContext> assignmentExpression() {
			return getRuleContexts(AssignmentExpressionContext.class);
		}
		public ArgumentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arguments; }
	}

	public final ArgumentsContext arguments() throws RecognitionException {
		ArgumentsContext _localctx = new ArgumentsContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_arguments);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(359); assignmentExpression();
			setState(364);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==25) {
				{
				{
				setState(360); match(25);
				setState(361); assignmentExpression();
				}
				}
				setState(366);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimaryExpressionContext extends ParserRuleContext {
		public TerminalNode CharacterConstant() { return getToken(GrammarParser.CharacterConstant, 0); }
		public IntegerConstantContext integerConstant() {
			return getRuleContext(IntegerConstantContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(GrammarParser.Identifier, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode StringLiteral() { return getToken(GrammarParser.StringLiteral, 0); }
		public PrimaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primaryExpression; }
	}

	public final PrimaryExpressionContext primaryExpression() throws RecognitionException {
		PrimaryExpressionContext _localctx = new PrimaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_primaryExpression);
		try {
			setState(375);
			switch (_input.LA(1)) {
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(367); match(Identifier);
				}
				break;
			case DecimalConstant:
			case OctalConstant:
			case HexadecimalConstant:
				enterOuterAlt(_localctx, 2);
				{
				setState(368); integerConstant();
				}
				break;
			case CharacterConstant:
				enterOuterAlt(_localctx, 3);
				{
				setState(369); match(CharacterConstant);
				}
				break;
			case StringLiteral:
				enterOuterAlt(_localctx, 4);
				{
				setState(370); match(StringLiteral);
				}
				break;
			case 28:
				enterOuterAlt(_localctx, 5);
				{
				setState(371); match(28);
				setState(372); expression();
				setState(373); match(16);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerConstantContext extends ParserRuleContext {
		public TerminalNode OctalConstant() { return getToken(GrammarParser.OctalConstant, 0); }
		public TerminalNode HexadecimalConstant() { return getToken(GrammarParser.HexadecimalConstant, 0); }
		public TerminalNode DecimalConstant() { return getToken(GrammarParser.DecimalConstant, 0); }
		public IntegerConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integerConstant; }
	}

	public final IntegerConstantContext integerConstant() throws RecognitionException {
		IntegerConstantContext _localctx = new IntegerConstantContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_integerConstant);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(377);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DecimalConstant) | (1L << OctalConstant) | (1L << HexadecimalConstant))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 8: return arrayDeclarator_sempred((ArrayDeclaratorContext)_localctx, predIndex);

		case 26: return arithmeticExpression_sempred((ArithmeticExpressionContext)_localctx, predIndex);

		case 29: return typeName_sempred((TypeNameContext)_localctx, predIndex);

		case 30: return postfixExpression_sempred((PostfixExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean typeName_sempred(TypeNameContext _localctx, int predIndex) {
		switch (predIndex) {
		case 11: return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean arithmeticExpression_sempred(ArithmeticExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1: return precpred(_ctx, 10);

		case 2: return precpred(_ctx, 9);

		case 3: return precpred(_ctx, 8);

		case 4: return precpred(_ctx, 7);

		case 5: return precpred(_ctx, 6);

		case 6: return precpred(_ctx, 5);

		case 7: return precpred(_ctx, 4);

		case 8: return precpred(_ctx, 3);

		case 9: return precpred(_ctx, 2);

		case 10: return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean arrayDeclarator_sempred(ArrayDeclaratorContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean postfixExpression_sempred(PostfixExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 17: return precpred(_ctx, 1);

		case 16: return precpred(_ctx, 2);

		case 12: return precpred(_ctx, 6);

		case 13: return precpred(_ctx, 5);

		case 14: return precpred(_ctx, 4);

		case 15: return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3F\u017e\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\3\2\3\2\3\2\3\3\3\3\3\3\7\3M\n\3\f\3\16\3P\13\3\3\4"+
		"\3\4\5\4T\n\4\3\4\3\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\7\6_\n\6\f\6\16\6b\13"+
		"\6\3\7\3\7\3\7\5\7g\n\7\3\b\3\b\3\t\3\t\3\t\5\tn\n\t\3\t\3\t\3\t\3\t\5"+
		"\tt\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\7\n~\n\n\f\n\16\n\u0081\13\n\3"+
		"\13\3\13\5\13\u0085\n\13\3\f\3\f\3\f\7\f\u008a\n\f\f\f\16\f\u008d\13\f"+
		"\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\5\16\u0097\n\16\3\16\3\16\3\16\3"+
		"\16\3\16\6\16\u009e\n\16\r\16\16\16\u009f\3\16\3\16\3\16\3\16\5\16\u00a6"+
		"\n\16\3\17\3\17\3\17\7\17\u00ab\n\17\f\17\16\17\u00ae\13\17\3\20\3\20"+
		"\3\20\3\20\3\20\3\20\5\20\u00b6\n\20\3\21\5\21\u00b9\n\21\3\21\3\21\3"+
		"\22\3\22\3\22\7\22\u00c0\n\22\f\22\16\22\u00c3\13\22\3\22\3\22\3\23\3"+
		"\23\3\23\3\23\3\23\3\23\3\23\5\23\u00ce\n\23\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\25\3\25\3\25\5\25\u00d9\n\25\3\25\3\25\5\25\u00dd\n\25\3\25\3"+
		"\25\5\25\u00e1\n\25\3\25\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31"+
		"\3\31\3\31\3\31\3\31\3\31\5\31\u00f2\n\31\3\31\5\31\u00f5\n\31\3\32\3"+
		"\32\3\32\7\32\u00fa\n\32\f\32\16\32\u00fd\13\32\3\33\3\33\3\33\3\33\3"+
		"\33\5\33\u0104\n\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\7\34\u0127\n\34\f\34\16"+
		"\34\u012a\13\34\3\35\3\35\3\35\3\35\3\35\3\35\5\35\u0132\n\35\3\36\3\36"+
		"\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\5\36\u013f\n\36\5\36\u0141"+
		"\n\36\3\37\3\37\3\37\3\37\3\37\7\37\u0148\n\37\f\37\16\37\u014b\13\37"+
		"\3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \5 \u0158\n \3 \3 \3 \3 \3 \3 \3 \3 "+
		"\3 \3 \3 \7 \u0165\n \f \16 \u0168\13 \3!\3!\3!\7!\u016d\n!\f!\16!\u0170"+
		"\13!\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\5\"\u017a\n\"\3#\3#\3#\2\6\22\66"+
		"<>$\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@B"+
		"D\2\f\4\2\20\20))\n\2\21\21\23\24\30\30\32\32\37\37##&\'\66\67\5\2\5\5"+
		"\16\1699\4\2\34\34..\4\2\13\13++\6\2\6\6\n\n\65\65;;\4\2\t\t88\4\2\7\7"+
		"**\b\2\3\3\5\5\26\26\34\34..::\3\2=?\u019b\2F\3\2\2\2\4N\3\2\2\2\6Q\3"+
		"\2\2\2\bW\3\2\2\2\n[\3\2\2\2\fc\3\2\2\2\16h\3\2\2\2\20s\3\2\2\2\22u\3"+
		"\2\2\2\24\u0084\3\2\2\2\26\u0086\3\2\2\2\30\u008e\3\2\2\2\32\u00a5\3\2"+
		"\2\2\34\u00a7\3\2\2\2\36\u00b5\3\2\2\2 \u00b8\3\2\2\2\"\u00bc\3\2\2\2"+
		"$\u00c6\3\2\2\2&\u00cf\3\2\2\2(\u00d5\3\2\2\2*\u00e5\3\2\2\2,\u00e7\3"+
		"\2\2\2.\u00e9\3\2\2\2\60\u00f4\3\2\2\2\62\u00f6\3\2\2\2\64\u0103\3\2\2"+
		"\2\66\u0105\3\2\2\28\u0131\3\2\2\2:\u0140\3\2\2\2<\u0142\3\2\2\2>\u014c"+
		"\3\2\2\2@\u0169\3\2\2\2B\u0179\3\2\2\2D\u017b\3\2\2\2FG\5\4\3\2GH\7\2"+
		"\2\3H\3\3\2\2\2IM\5\6\4\2JM\5\b\5\2KM\7\62\2\2LI\3\2\2\2LJ\3\2\2\2LK\3"+
		"\2\2\2MP\3\2\2\2NL\3\2\2\2NO\3\2\2\2O\5\3\2\2\2PN\3\2\2\2QS\5\32\16\2"+
		"RT\5\n\6\2SR\3\2\2\2ST\3\2\2\2TU\3\2\2\2UV\7\62\2\2V\7\3\2\2\2WX\5\32"+
		"\16\2XY\5\20\t\2YZ\5\"\22\2Z\t\3\2\2\2[`\5\f\7\2\\]\7\33\2\2]_\5\f\7\2"+
		"^\\\3\2\2\2_b\3\2\2\2`^\3\2\2\2`a\3\2\2\2a\13\3\2\2\2b`\3\2\2\2cf\5\20"+
		"\t\2de\7\23\2\2eg\5\16\b\2fd\3\2\2\2fg\3\2\2\2g\r\3\2\2\2hi\5\64\33\2"+
		"i\17\3\2\2\2jk\7<\2\2km\7\36\2\2ln\5\26\f\2ml\3\2\2\2mn\3\2\2\2no\3\2"+
		"\2\2ot\7\22\2\2pq\7\5\2\2qt\5\20\t\2rt\5\22\n\2sj\3\2\2\2sp\3\2\2\2sr"+
		"\3\2\2\2t\21\3\2\2\2uv\b\n\1\2vw\7<\2\2w\177\3\2\2\2xy\f\3\2\2yz\7\4\2"+
		"\2z{\5\24\13\2{|\7\31\2\2|~\3\2\2\2}x\3\2\2\2~\u0081\3\2\2\2\177}\3\2"+
		"\2\2\177\u0080\3\2\2\2\u0080\23\3\2\2\2\u0081\177\3\2\2\2\u0082\u0085"+
		"\5D#\2\u0083\u0085\7/\2\2\u0084\u0082\3\2\2\2\u0084\u0083\3\2\2\2\u0085"+
		"\25\3\2\2\2\u0086\u008b\5\30\r\2\u0087\u0088\7\33\2\2\u0088\u008a\5\30"+
		"\r\2\u0089\u0087\3\2\2\2\u008a\u008d\3\2\2\2\u008b\u0089\3\2\2\2\u008b"+
		"\u008c\3\2\2\2\u008c\27\3\2\2\2\u008d\u008b\3\2\2\2\u008e\u008f\5\32\16"+
		"\2\u008f\u0090\5\20\t\2\u0090\31\3\2\2\2\u0091\u00a6\7\"\2\2\u0092\u00a6"+
		"\7\r\2\2\u0093\u00a6\7!\2\2\u0094\u0096\t\2\2\2\u0095\u0097\7<\2\2\u0096"+
		"\u0095\3\2\2\2\u0096\u0097\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u009d\7$"+
		"\2\2\u0099\u009a\5\32\16\2\u009a\u009b\5\34\17\2\u009b\u009c\7\62\2\2"+
		"\u009c\u009e\3\2\2\2\u009d\u0099\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u009d"+
		"\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\u00a2\7\f\2\2\u00a2"+
		"\u00a6\3\2\2\2\u00a3\u00a4\t\2\2\2\u00a4\u00a6\7<\2\2\u00a5\u0091\3\2"+
		"\2\2\u00a5\u0092\3\2\2\2\u00a5\u0093\3\2\2\2\u00a5\u0094\3\2\2\2\u00a5"+
		"\u00a3\3\2\2\2\u00a6\33\3\2\2\2\u00a7\u00ac\5\20\t\2\u00a8\u00a9\7\33"+
		"\2\2\u00a9\u00ab\5\20\t\2\u00aa\u00a8\3\2\2\2\u00ab\u00ae\3\2\2\2\u00ac"+
		"\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad\35\3\2\2\2\u00ae\u00ac\3\2\2"+
		"\2\u00af\u00b6\5 \21\2\u00b0\u00b6\5\"\22\2\u00b1\u00b6\5$\23\2\u00b2"+
		"\u00b6\5&\24\2\u00b3\u00b6\5(\25\2\u00b4\u00b6\5\60\31\2\u00b5\u00af\3"+
		"\2\2\2\u00b5\u00b0\3\2\2\2\u00b5\u00b1\3\2\2\2\u00b5\u00b2\3\2\2\2\u00b5"+
		"\u00b3\3\2\2\2\u00b5\u00b4\3\2\2\2\u00b6\37\3\2\2\2\u00b7\u00b9\5\62\32"+
		"\2\u00b8\u00b7\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba\u00bb"+
		"\7\62\2\2\u00bb!\3\2\2\2\u00bc\u00c1\7$\2\2\u00bd\u00c0\5\6\4\2\u00be"+
		"\u00c0\5\36\20\2\u00bf\u00bd\3\2\2\2\u00bf\u00be\3\2\2\2\u00c0\u00c3\3"+
		"\2\2\2\u00c1\u00bf\3\2\2\2\u00c1\u00c2\3\2\2\2\u00c2\u00c4\3\2\2\2\u00c3"+
		"\u00c1\3\2\2\2\u00c4\u00c5\7\f\2\2\u00c5#\3\2\2\2\u00c6\u00c7\7 \2\2\u00c7"+
		"\u00c8\7\36\2\2\u00c8\u00c9\5\62\32\2\u00c9\u00ca\7\22\2\2\u00ca\u00cd"+
		"\5\36\20\2\u00cb\u00cc\7(\2\2\u00cc\u00ce\5\36\20\2\u00cd\u00cb\3\2\2"+
		"\2\u00cd\u00ce\3\2\2\2\u00ce%\3\2\2\2\u00cf\u00d0\7\35\2\2\u00d0\u00d1"+
		"\7\36\2\2\u00d1\u00d2\5\62\32\2\u00d2\u00d3\7\22\2\2\u00d3\u00d4\5\36"+
		"\20\2\u00d4\'\3\2\2\2\u00d5\u00d6\7\60\2\2\u00d6\u00d8\7\36\2\2\u00d7"+
		"\u00d9\5*\26\2\u00d8\u00d7\3\2\2\2\u00d8\u00d9\3\2\2\2\u00d9\u00da\3\2"+
		"\2\2\u00da\u00dc\7\62\2\2\u00db\u00dd\5,\27\2\u00dc\u00db\3\2\2\2\u00dc"+
		"\u00dd\3\2\2\2\u00dd\u00de\3\2\2\2\u00de\u00e0\7\62\2\2\u00df\u00e1\5"+
		".\30\2\u00e0\u00df\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2"+
		"\u00e3\7\22\2\2\u00e3\u00e4\5\36\20\2\u00e4)\3\2\2\2\u00e5\u00e6\5\62"+
		"\32\2\u00e6+\3\2\2\2\u00e7\u00e8\5\62\32\2\u00e8-\3\2\2\2\u00e9\u00ea"+
		"\5\62\32\2\u00ea/\3\2\2\2\u00eb\u00ec\7\b\2\2\u00ec\u00f5\7\62\2\2\u00ed"+
		"\u00ee\7%\2\2\u00ee\u00f5\7\62\2\2\u00ef\u00f1\7\61\2\2\u00f0\u00f2\5"+
		"\62\32\2\u00f1\u00f0\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2\u00f3\3\2\2\2\u00f3"+
		"\u00f5\7\62\2\2\u00f4\u00eb\3\2\2\2\u00f4\u00ed\3\2\2\2\u00f4\u00ef\3"+
		"\2\2\2\u00f5\61\3\2\2\2\u00f6\u00fb\5\64\33\2\u00f7\u00f8\7\33\2\2\u00f8"+
		"\u00fa\5\64\33\2\u00f9\u00f7\3\2\2\2\u00fa\u00fd\3\2\2\2\u00fb\u00f9\3"+
		"\2\2\2\u00fb\u00fc\3\2\2\2\u00fc\63\3\2\2\2\u00fd\u00fb\3\2\2\2\u00fe"+
		"\u0104\5\66\34\2\u00ff\u0100\5:\36\2\u0100\u0101\t\3\2\2\u0101\u0102\5"+
		"\64\33\2\u0102\u0104\3\2\2\2\u0103\u00fe\3\2\2\2\u0103\u00ff\3\2\2\2\u0104"+
		"\65\3\2\2\2\u0105\u0106\b\34\1\2\u0106\u0107\58\35\2\u0107\u0128\3\2\2"+
		"\2\u0108\u0109\f\f\2\2\u0109\u010a\t\4\2\2\u010a\u0127\5\66\34\r\u010b"+
		"\u010c\f\13\2\2\u010c\u010d\t\5\2\2\u010d\u0127\5\66\34\f\u010e\u010f"+
		"\f\n\2\2\u010f\u0110\t\6\2\2\u0110\u0127\5\66\34\13\u0111\u0112\f\t\2"+
		"\2\u0112\u0113\t\7\2\2\u0113\u0127\5\66\34\n\u0114\u0115\f\b\2\2\u0115"+
		"\u0116\t\b\2\2\u0116\u0127\5\66\34\t\u0117\u0118\f\7\2\2\u0118\u0119\7"+
		"\3\2\2\u0119\u0127\5\66\34\b\u011a\u011b\f\6\2\2\u011b\u011c\7,\2\2\u011c"+
		"\u0127\5\66\34\7\u011d\u011e\f\5\2\2\u011e\u011f\7\25\2\2\u011f\u0127"+
		"\5\66\34\6\u0120\u0121\f\4\2\2\u0121\u0122\7\63\2\2\u0122\u0127\5\66\34"+
		"\5\u0123\u0124\f\3\2\2\u0124\u0125\7\64\2\2\u0125\u0127\5\66\34\4\u0126"+
		"\u0108\3\2\2\2\u0126\u010b\3\2\2\2\u0126\u010e\3\2\2\2\u0126\u0111\3\2"+
		"\2\2\u0126\u0114\3\2\2\2\u0126\u0117\3\2\2\2\u0126\u011a\3\2\2\2\u0126"+
		"\u011d\3\2\2\2\u0126\u0120\3\2\2\2\u0126\u0123\3\2\2\2\u0127\u012a\3\2"+
		"\2\2\u0128\u0126\3\2\2\2\u0128\u0129\3\2\2\2\u0129\67\3\2\2\2\u012a\u0128"+
		"\3\2\2\2\u012b\u0132\5:\36\2\u012c\u012d\7\36\2\2\u012d\u012e\5<\37\2"+
		"\u012e\u012f\7\22\2\2\u012f\u0130\58\35\2\u0130\u0132\3\2\2\2\u0131\u012b"+
		"\3\2\2\2\u0131\u012c\3\2\2\2\u01329\3\2\2\2\u0133\u0141\5> \2\u0134\u0135"+
		"\t\t\2\2\u0135\u0141\5:\36\2\u0136\u0137\t\n\2\2\u0137\u0141\58\35\2\u0138"+
		"\u013e\7\27\2\2\u0139\u013f\5:\36\2\u013a\u013b\7\36\2\2\u013b\u013c\5"+
		"<\37\2\u013c\u013d\7\22\2\2\u013d\u013f\3\2\2\2\u013e\u0139\3\2\2\2\u013e"+
		"\u013a\3\2\2\2\u013f\u0141\3\2\2\2\u0140\u0133\3\2\2\2\u0140\u0134\3\2"+
		"\2\2\u0140\u0136\3\2\2\2\u0140\u0138\3\2\2\2\u0141;\3\2\2\2\u0142\u0143"+
		"\b\37\1\2\u0143\u0144\5\32\16\2\u0144\u0149\3\2\2\2\u0145\u0146\f\3\2"+
		"\2\u0146\u0148\7\5\2\2\u0147\u0145\3\2\2\2\u0148\u014b\3\2\2\2\u0149\u0147"+
		"\3\2\2\2\u0149\u014a\3\2\2\2\u014a=\3\2\2\2\u014b\u0149\3\2\2\2\u014c"+
		"\u014d\b \1\2\u014d\u014e\5B\"\2\u014e\u0166\3\2\2\2\u014f\u0150\f\b\2"+
		"\2\u0150\u0151\7\4\2\2\u0151\u0152\5\62\32\2\u0152\u0153\7\31\2\2\u0153"+
		"\u0165\3\2\2\2\u0154\u0155\f\7\2\2\u0155\u0157\7\36\2\2\u0156\u0158\5"+
		"@!\2\u0157\u0156\3\2\2\2\u0157\u0158\3\2\2\2\u0158\u0159\3\2\2\2\u0159"+
		"\u0165\7\22\2\2\u015a\u015b\f\6\2\2\u015b\u015c\7-\2\2\u015c\u0165\7<"+
		"\2\2\u015d\u015e\f\5\2\2\u015e\u015f\7\17\2\2\u015f\u0165\7<\2\2\u0160"+
		"\u0161\f\4\2\2\u0161\u0165\7*\2\2\u0162\u0163\f\3\2\2\u0163\u0165\7\7"+
		"\2\2\u0164\u014f\3\2\2\2\u0164\u0154\3\2\2\2\u0164\u015a\3\2\2\2\u0164"+
		"\u015d\3\2\2\2\u0164\u0160\3\2\2\2\u0164\u0162\3\2\2\2\u0165\u0168\3\2"+
		"\2\2\u0166\u0164\3\2\2\2\u0166\u0167\3\2\2\2\u0167?\3\2\2\2\u0168\u0166"+
		"\3\2\2\2\u0169\u016e\5\64\33\2\u016a\u016b\7\33\2\2\u016b\u016d\5\64\33"+
		"\2\u016c\u016a\3\2\2\2\u016d\u0170\3\2\2\2\u016e\u016c\3\2\2\2\u016e\u016f"+
		"\3\2\2\2\u016fA\3\2\2\2\u0170\u016e\3\2\2\2\u0171\u017a\7<\2\2\u0172\u017a"+
		"\5D#\2\u0173\u017a\7@\2\2\u0174\u017a\7A\2\2\u0175\u0176\7\36\2\2\u0176"+
		"\u0177\5\62\32\2\u0177\u0178\7\22\2\2\u0178\u017a\3\2\2\2\u0179\u0171"+
		"\3\2\2\2\u0179\u0172\3\2\2\2\u0179\u0173\3\2\2\2\u0179\u0174\3\2\2\2\u0179"+
		"\u0175\3\2\2\2\u017aC\3\2\2\2\u017b\u017c\t\13\2\2\u017cE\3\2\2\2\'LN"+
		"S`fms\177\u0084\u008b\u0096\u009f\u00a5\u00ac\u00b5\u00b8\u00bf\u00c1"+
		"\u00cd\u00d8\u00dc\u00e0\u00f1\u00f4\u00fb\u0103\u0126\u0128\u0131\u013e"+
		"\u0140\u0149\u0157\u0164\u0166\u016e\u0179";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}