grammar Grammar;
@header {package compiler.syntactic;} 

compilationUnit
    : program EOF
    ;

program
    : (declaration | functionDefinition | ';')*
    ;

declaration
    : typeSpecifier initDeclaratorList? ';'
    ;

functionDefinition
    : typeSpecifier declarator compoundStatement
    ;

initDeclaratorList
    : initDeclarator (',' initDeclarator)*
    ;

initDeclarator
    : declarator ('=' initializer)?
    ;
 
initializer
    : assignmentExpression
    ;
 
declarator
    : Identifier '(' parameterList? ')'
    | '*' declarator
    | arrayDeclarator
    ;

arrayDeclarator
    : Identifier
    | arrayDeclarator '[' arrayCapacity ']'
    ;

arrayCapacity
    : integerConstant
    | '8 + 8 - 1'
    ;

parameterList
    : parameterDeclaration (',' parameterDeclaration)*
    ;

parameterDeclaration
    : typeSpecifier declarator
    ;

typeSpecifier
    : 'void' 
    | 'char' 
    | 'int' 
    | ('struct' | 'union') Identifier? '{' (typeSpecifier declaratorList ';')+ '}'
    | ('struct' | 'union')  Identifier
    ;
 
declaratorList
    : declarator (',' declarator)*
    ;

statement
    : expressionStatement
    | compoundStatement
    | selectionStatement
    | whileStatement
    | forStatement
    | jumpStatement
    ;
 
expressionStatement
    : expression? ';'
    ;
 
compoundStatement
    : '{' (declaration | statement)* '}'
    ;
 
selectionStatement
    : 'if' '(' expression ')' statement ('else' statement)?
    ;
 
whileStatement
    : 'while' '(' expression ')' statement
    ;

forStatement
    : 'for' '(' expression1? ';' expression2? ';' expression3? ')' statement
    ;

expression1
    : expression
    ;
 
expression2
    : expression
    ;

expression3
    : expression
    ;

jumpStatement
    : 'continue' ';'
    | 'break' ';'
    | 'return' expression? ';'
    ;

expression
    : assignmentExpression (',' assignmentExpression)*
    ;

assignmentExpression
    : arithmeticExpression
    | unaryExpression ('=' | '*=' | '/=' | '%=' | '+=' | '-=' | '<<=' | '>>=' | '&=' | '^=' | '|=') assignmentExpression
    ;

arithmeticExpression
    : castExpression
    | arithmeticExpression ('*' | '/' | '%') arithmeticExpression
    | arithmeticExpression ('+' | '-') arithmeticExpression
    | arithmeticExpression ('<<' | '>>') arithmeticExpression
    | arithmeticExpression ('<' | '>' | '<=' | '>=') arithmeticExpression
    | arithmeticExpression ('==' | '!=') arithmeticExpression
    | arithmeticExpression '&' arithmeticExpression
    | arithmeticExpression '^' arithmeticExpression
    | arithmeticExpression '|' arithmeticExpression
    | arithmeticExpression '&&' arithmeticExpression
    | arithmeticExpression '||' arithmeticExpression
    ;

castExpression
    : unaryExpression
    | '(' typeName ')' castExpression
    ;

unaryExpression
    : postfixExpression
    | ('++' | '--') unaryExpression
    | ('&' | '*' | '+' | '-' | '~' | '!') castExpression
    | 'sizeof' (unaryExpression | '(' typeName ')')
    ;

typeName
    : typeSpecifier
    | typeName '*'
    ;

postfixExpression
    : primaryExpression
    | postfixExpression '[' expression ']'
    | postfixExpression '(' arguments? ')'
    | postfixExpression '.' Identifier
    | postfixExpression '->' Identifier
    | postfixExpression '++'
    | postfixExpression '--'
    ;

arguments
    : assignmentExpression (',' assignmentExpression)*
    ;

primaryExpression
    : Identifier
    | integerConstant
    | CharacterConstant
    | StringLiteral
    | '(' expression ')'
    ;

integerConstant
    : DecimalConstant
    | OctalConstant
    | HexadecimalConstant
    ;

Identifier
    : Nondigit (Nondigit | Digit)*
    ;

fragment
Nondigit
    : [a-zA-Z_$]
    ;

fragment
Digit
    : [0-9]
    ;

DecimalConstant
    : NonzeroDigit Digit*
    ;

OctalConstant
    : '0' OctalDigit*
    ;

HexadecimalConstant
    : '0' [xX] HexadecimalDigit+
    ;

fragment
NonzeroDigit
    : [1-9]
    ;

fragment
OctalDigit
    : [0-7]
    ;

fragment
HexadecimalDigit
    : [0-9a-fA-F]
    ;

CharacterConstant
    : '\'' CChar '\''
    ;

fragment
CChar
    : ~['\\\r\n]
    | EscapeSequence    
    ;

fragment
EscapeSequence
    : SimpleEscapeSequence
    | OctalEscapeSequence
    | HexadecimalEscapeSequence
    ;

fragment
SimpleEscapeSequence
    : '\\' ['"?abfnrtv\\]
    ;

fragment
OctalEscapeSequence
    : '\\' OctalDigit
    | '\\' OctalDigit OctalDigit
    | '\\' OctalDigit OctalDigit OctalDigit
    ;

fragment
HexadecimalEscapeSequence
    : '\\x' HexadecimalDigit+
    ;

StringLiteral
    : '"' SCharSequence? '"'
    ;

fragment
SCharSequence
    : SChar+
    ;

fragment
SChar
    : ~["\\\r\n]
    | EscapeSequence
    ;

Whitespace
    : [ \t]+ -> skip
    ;

Newline
    : ('\r' '\n'? | '\n') -> skip
    ;

BlockComment
    : '/*' .*? '*/' -> skip
    ;

LineComment
    : '//' ~[\r\n]* -> skip
    ;

IncludeLine
    : '#' ~[\r\n]* -> skip
    ;

