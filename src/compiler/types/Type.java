package compiler.types;

public abstract class Type {
    public static Void VOID = new Void();
    int size;
    boolean isLvalue;
    public boolean isLvalue() {
        return isLvalue;
    }
    public int size() {
        return size;
    }
    final public Type toNonLvalue() {
        Type res = clone();
        res.isLvalue = false;
        return res;
    }
    @Override
    public abstract boolean equals(Object object);
    @Override 
    public abstract Type clone();
}
