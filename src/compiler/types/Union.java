package compiler.types;

import java.util.*;

public final class Union extends Record {
    public Union(boolean isLvalue, String name, ArrayList<String> fields, ArrayList<Type> types) {
        super(isLvalue, name, fields, types);
        calcSize();
    }
    @Override
    public boolean equals(Object object) {
        return object instanceof Union && ((Union)object).name.equals(name);
    }
    @Override 
    public Union clone() {
        return new Union(isLvalue, name, fields, types);
    }
    @Override
    public void calcSize() {
        offsets = new ArrayList<Integer>();
        size = 0;
        for (int i = 0; i < fields.size(); ++i) {
            offsets.add(0);
            if (types.get(i).size() > size) {
                size = types.get(i).size();
            }
        }
    }
} 

