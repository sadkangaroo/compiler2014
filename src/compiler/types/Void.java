package compiler.types;

public final class Void extends Type {
    public Void() {
        size = 0;
        isLvalue = false;
    }
    @Override 
    public Void clone() {
        return this;
    }
    @Override
    public boolean equals(Object object) {
        return object instanceof Void;
    }
}

