package compiler.types;

import java.util.*;

public final class Struct extends Record {
    public Struct(boolean isLvalue, String name, ArrayList<String> fields, ArrayList<Type> types) {
        super(isLvalue, name, fields, types);
    }
    @Override
    public boolean equals(Object object) {
        return object instanceof Struct && ((Struct)object).name.equals(name);
    }
    @Override 
    public Struct clone() {
        return new Struct(isLvalue, name, fields, types);
    }
    @Override
    public void calcSize() {
        offsets = new ArrayList<Integer>();
        size = 0;
        for (int i = 0; i < fields.size(); ++i) {
            offsets.add(size);
            size = size + types.get(i).size();
        }
    }
} 

