package compiler.types;

import java.util.*;

import compiler.variable.*;

public final class Function extends Type {
    public Type returnType;
    public Variable rReservation;
    public ArrayList<Type> parameterTypes;
    public ArrayList<Variable> pReservation;
    public ArrayList<String> parameterNames;
    public Function(Type returnType, ArrayList<Variable> parameters) {
        size = 4;
        isLvalue = false;
        this.returnType = returnType;
        parameterTypes = new ArrayList<Type>();
        parameterNames = new ArrayList<String>();
        for (int i = 0; i < parameters.size(); ++i) {
            parameterTypes.add(parameters.get(i).type());
            parameterNames.add(parameters.get(i).name());
        }
        pReservation = new ArrayList<Variable>();
    }
    @Override
    public boolean equals(Object object) {
        return object instanceof Function
            && ((Function)object).parameterTypes.equals(parameterTypes)
            && ((Function)object).returnType.equals(returnType);
    }
    @Override
    public Function clone() {
        return this;
    }
}
