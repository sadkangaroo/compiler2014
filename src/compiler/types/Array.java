package compiler.types;

public final class Array extends Type {
    Type elementType;
    int capacity;
    public Array(Type elementType, int capacity) {
        this.elementType = elementType;
        isLvalue = false; 
        size = elementType.size * capacity;
        this.capacity = capacity;
    }
    public Type elementType() {
        return elementType;
    }
    @Override 
    public Array clone() {
        return new Array((Type)elementType.clone(), capacity);
    }
    @Override
    public boolean equals(Object object) {
        return object instanceof Array;
    }
}
