package compiler.types;

import java.util.*;

public abstract class Record extends Type{
    String name;
    ArrayList<String> fields;
    ArrayList<Type> types;
    ArrayList<Integer> offsets;
    public Record(boolean isLvalue, String name, ArrayList<String> fields, ArrayList<Type> types) {
        this.isLvalue = isLvalue;
        this.name = name;
        this.fields = fields;
        this.types = types;
    }
    public String name() {
        return name;
    }
    public boolean contains(String name) {
        return fields.contains(name);
    }
    public int getOffset(String name) {
        return offsets.get(fields.indexOf(name));
    }
    public Type getType(String name) {
        return types.get(fields.indexOf(name));
    }
    public abstract void calcSize();
}

