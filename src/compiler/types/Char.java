package compiler.types;

public final class Char extends Num {
    public Char(boolean isLvalue) {
        size = 4;
        this.isLvalue = isLvalue;
    }
    public Char() {
        size = 4;
        isLvalue = false;
    }
    @Override
    public Char clone() {
        return new Char(isLvalue);
    }
}

