package compiler.types;

public class Pointer extends Arithmetic {
    Type elementType;
    public Pointer(Type elementType, boolean isLvalue) {
        size = 4;
        this.elementType = elementType;
        this.isLvalue = isLvalue;
    }
    public Pointer(Type elementType) {
        size = 4;
        this.elementType = elementType;
        isLvalue = false;
    }
    public Type elementType() {
        return elementType;
    }
    @Override 
    public Pointer clone() {
        return new Pointer((Type)elementType.clone(), isLvalue);
    }
}
