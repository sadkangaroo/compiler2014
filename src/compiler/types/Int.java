package compiler.types;

public final class Int extends Num {
    public Int(boolean isLvalue) {
        size = 4;
        this.isLvalue = isLvalue;
    }
    public Int() {
        size = 4;
        isLvalue = false;
    }
    @Override 
    public Int clone() {
        return new Int(isLvalue);
    }
}

