package compiler.types; 

public abstract class Arithmetic extends Type {
    @Override 
    public boolean equals(Object object) {
        return object instanceof Arithmetic;
    }
}
