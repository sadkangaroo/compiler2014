package compiler.semantic;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.util.*;

import compiler.syntactic.GrammarParser.*;
import compiler.errors.*;
import compiler.types.*;
import compiler.table.*;
import compiler.code.*;
import compiler.variable.*;
import compiler.writer.*;
import compiler.optimizer.*;

public class Translator {
    Env env;
    Label cntFuncLabel;
    Function cntFunc;
    SpaceOffset raBak;
    HashMap<String, Record> def;
    HashMap<SpaceOffset, SpaceOffset> pt;
    boolean isGlobal;
    int anonymousCounter;
    Writer writer;
    public Translator(String libPath) {
        env = new Env();
        def = new HashMap<String, Record>();
        pt = new HashMap<SpaceOffset, SpaceOffset>();
        anonymousCounter = 0;
        cntFuncLabel = null;
        isGlobal = true;
        Writer.libPath = libPath;
    }
    public void translate(ParseTree tree) {
        visitProgram(((CompilationUnitContext)tree).program());
        Writer.write();
    }
    void add(Tac sentence) {
        Code.put(sentence, isGlobal);
    }
    SpaceOffset get(Type type) {
        return new SpaceOffset(type, isGlobal);
    }
    Pointer toPointer(Type type) {
        Type elementType = ((Array)type).elementType();
        return new Pointer(elementType);
    }
    Variable toPointer(Variable var) {
        if (var instanceof SpaceOffset && ((SpaceOffset)var).isGlobal()) {
            SpaceOffset arr = (SpaceOffset)var;
            if (pt.containsKey(arr)) {
                return pt.get(arr);
            }
            Type elementType = ((Array)arr.type()).elementType();
            SpaceOffset tmp = new SpaceOffset(new Pointer(elementType, true), true);
            Code.put(new Addr(tmp, arr), true);
            pt.put(arr, tmp);
            return tmp;
        }
        Type elementType = ((Array)var.type()).elementType();
        SpaceOffset tmp = get(new Pointer(elementType, true));
        add(new Addr(tmp, var));
        return tmp;
    }
    public static boolean isComp(String op) {
        return op.equals("==") ||op.equals("!=") || op.equals(">=") || op.equals("<=") || op.equals(">") ||op.equals("<");
    }
    void generateCall(Label callee, Label caller, Function funcType, ArrayList<Variable> arguments, ParserRuleContext ctx) {

        if (callee.num() == 1) {
            generateMove(Register.A0, arguments.get(0), ctx);
            add(new Malloc());
        }
        else if (callee.num() == 2) {
            generateMove(new Register("$a2"), arguments.get(0), ctx);
            for (int i = 1; i < arguments.size(); ++i) {
                generateMove(new SpaceOffset(new Int(), true, -32768 + i * 4), arguments.get(i), ctx);
            }
            add(new Printf());
        }
        else {
            for (int i = 0; i < arguments.size(); ++i) {
                generateMove(funcType.pReservation.get(i), arguments.get(i), ctx);
            }
            add(new PushSp(caller.num(), true));
            add(new Jal(callee)); 
            add(new ResetSp(caller.num(), true));
        }
    }
    void generateMove(Variable left, Variable right, ParserRuleContext ctx) {
        if (left.type() == Type.VOID) {
            return;
        }
        if (right.type() instanceof Array) {
            right = toPointer(right);
        }
        if (!right.type().equals(left.type())) {
            Errors.reportIncompatible(ctx);
        }
        if (left.type() instanceof Record) {
            int total = left.type().size() / 4;
            SpaceOffset leftStart = get(new Int());
            SpaceOffset rightStart = get(new Int());
            add(new Addr(leftStart, left));
            add(new Addr(rightStart, right));
            for (int i = 0; i < total; ++i) {
                SpaceVar leftWord = new SpaceVar(new Int(), leftStart, i * 4);
                SpaceVar rightWord = new SpaceVar(new Int(), rightStart, i * 4);
                add(new Move(leftWord, rightWord));
            }
        }
        else {
            if (left instanceof SpaceOffset && right instanceof SpaceOffset) {
                ((SpaceOffset)left).str = ((SpaceOffset)right).str;
            }
            add(new Move(left, right));
        }
    }
    SpaceOffset generateCalc(String op, Variable left, Variable right, ParserRuleContext ctx, Label trueTarget, Label falseTarget) {
        if (left.type() instanceof Array) {
            left = toPointer(left);
        }
        if (right.type() instanceof Array) {
            right = toPointer(right);
        }
        if (!(left.type() instanceof Arithmetic) || !(right.type() instanceof Arithmetic)) {
            Errors.reportInvalidOp(ctx);
        }
        if (left.type() instanceof Num && right.type() instanceof Num || isComp(op)) {
            SpaceOffset var = get(new Int());
            add(new Calc(op, var, left, right, trueTarget, falseTarget));
            return var;
        }
        if (!op.equals("+") && !op.equals("-")) {
            Errors.reportInvalidOp(ctx);
        }
        if (!(left.type() instanceof Pointer) || !(right.type() instanceof Num)) {
            Errors.reportInvalidOp(ctx);
        }
        SpaceOffset temp = get(new Int());
        int elementSize = ((Pointer)left.type()).elementType().size();
        add(new Calc("*", temp, right, new ImmNumber(elementSize), null, null));
        SpaceOffset res = get(left.type());
        add(new Calc(op, res, left, temp, null, null));
        return res;
    }
    void addPredefinedFunction(ParserRuleContext ctx) {
        env.put("malloc", get(new Function(new Pointer(Type.VOID), new ArrayList<Variable>())), ctx, def);
        env.put("printf", get(new Function(new Int(), new ArrayList<Variable>())), ctx, def);
    }
    void visitProgram(ProgramContext ctx) {
        env.beginScope();
        addPredefinedFunction(ctx);        
        for (int i = 0; i < ctx.getChildCount(); ++i) {
            ParseTree child = ctx.getChild(i);
            if (child instanceof DeclarationContext) {
                visitDeclaration((DeclarationContext)child);
            }
            if (child instanceof FunctionDefinitionContext) {
                visitFunctionDefinition((FunctionDefinitionContext)child);
            }
        }
        env.endScope();
    }
    void visitDeclaration(DeclarationContext ctx) {
        Type type = visitTypeSpecifier(ctx.typeSpecifier());
        if (ctx.initDeclaratorList() != null) {
            visitInitDeclaratorList(ctx.initDeclaratorList(), type);
        }
    }
    void visitInitDeclaratorList(InitDeclaratorListContext ctx, Type type) {
        Iterator<InitDeclaratorContext> iterator = ctx.initDeclarator().iterator();
        while (iterator.hasNext()) {
            visitInitDeclarator(iterator.next(), type); 
        }
    }
    SpaceOffset visitDeclarator(DeclaratorContext ctx, Type type) {
        if (ctx.arrayDeclarator() != null) {
            return visitArrayDeclarator(ctx.arrayDeclarator(), type);
        }
        else if (ctx.declarator() != null) {
            return visitDeclarator(ctx.declarator(), new Pointer(type, true));
        }
        else {
            ArrayList<Variable> parameters = visitParameterList(ctx.parameterList());
            return visitDeclaratorIdentifier(ctx.Identifier().getText(), new Function(type, parameters), ctx);
        }
    }
    SpaceOffset visitDeclaratorIdentifier(String identifier, Type type, ParserRuleContext ctx) {
        if (type instanceof Record) {
            String name = ((Record)type).name();
            if (!(def.containsKey(name) || !def.get(name).equals(type))) {
                Errors.reportTypeNotDefined(ctx);
            }
        }
        if (type instanceof Function) {
            Type returnType = ((Function)type).returnType;
            if (returnType instanceof Record) {
                String name = ((Record)returnType).name();
                if (!(def.containsKey(name) || !def.get(name).equals(returnType))) {
                    Errors.reportTypeNotDefined(ctx);
                }
            }
        }
        SpaceOffset res = get(type);
        env.put(identifier, res, ctx, def);
        return res;
    }
    ArrayList<Variable> visitParameterList(ParameterListContext ctx) {
        ArrayList<Variable> parameters = new ArrayList<Variable>();
        if (ctx == null) {
            return parameters;
        }
        Iterator<ParameterDeclarationContext> iterator = ctx.parameterDeclaration().iterator();
        while (iterator.hasNext()) {
            ParameterDeclarationContext parameterElement = iterator.next();
            Type parameterType = visitTypeSpecifier(parameterElement.typeSpecifier());
            Variable parameter = visitDeclarator(parameterElement.declarator(), parameterType);
            if (parameter.type() instanceof Array) {
                parameter.setType(toPointer(parameter.type()));
            }
            parameters.add(parameter);
        }
        return parameters;
    }
    void visitInitDeclarator(InitDeclaratorContext ctx, Type type) {
        Variable leftVar = visitDeclarator(ctx.declarator(), type);
        if (ctx.initializer() != null) {
            Variable rightVar = visitInitializer(ctx.initializer()); 
            if (!leftVar.type().isLvalue()) {
                Errors.reportNotLvalue(ctx);
            }
            generateMove(leftVar, rightVar, ctx);
        }
    }
    Type visitTypeSpecifier(TypeSpecifierContext ctx) {
        String typeName = ctx.getChild(0).getText();
        int cnt = ctx.getChildCount();
        if (typeName.equals("void")) {
            return Type.VOID;
        }
        else if (typeName.equals("int")) {
            return new Int(true);
        }
        else if (typeName.equals("char")) {
            return new Char(true);
        }
        else {
            String name;
            if (ctx.Identifier() != null) {
                name = ctx.Identifier().getText(); 
            }
            else {
                name = String.valueOf(anonymousCounter);
            }                    
            if (cnt == 2) {
                if (typeName.equals("struct")) {
                    if (def.containsKey(name) && def.get(name) instanceof Struct) {
                        return def.get(name);
                    }
                    else {
                        return new Struct(true, name, new ArrayList<String>(), new ArrayList<Type>());
                    }
                }
                else {
                    if (def.containsKey(name) && def.get(name) instanceof Union) {
                        return def.get(name);
                    }
                    else {
                        return new Union(true, name, new ArrayList<String>(), new ArrayList<Type>());
                    }
                }
            }
            else {
                if (def.containsKey(name)) {
                    Errors.reportRedefinition(ctx);
                }
                Record record;
                ArrayList<String> fields = new ArrayList<String>();
                ArrayList<Type> types = new ArrayList<Type>();
                if (typeName.equals("struct")) {
                    record = new Struct(true, name, fields, types);
                }
                else {
                    record = new Union(true, name, fields, types);
                }
                def.put(name, record);
                Iterator<TypeSpecifierContext> iteratorSpecifier = ctx.typeSpecifier().iterator();
                Iterator<DeclaratorListContext> iteratorDeclaratorList = ctx.declaratorList().iterator();
                env.beginScope();
                while (iteratorSpecifier.hasNext()) {
                    Type type = visitTypeSpecifier(iteratorSpecifier.next());
                    Iterator<DeclaratorContext> iteratorDeclarator = iteratorDeclaratorList.next().declarator().iterator();
                    while (iteratorDeclarator.hasNext()) {
                        Variable element = visitDeclarator(iteratorDeclarator.next(), type); 
                        fields.add(element.name());
                        types.add(element.type());
                    }
                }
                record.calcSize();
                env.endScope();
                return record;
            }
        }
    }
    void visitFunctionDefinition(FunctionDefinitionContext ctx) {
        env.beginScope();
        isGlobal = false;
        int bak = SpaceOffset.spCnt;
        Type type = visitTypeSpecifier(ctx.typeSpecifier());
        SpaceOffset func = visitDeclarator(ctx.declarator(), type);
        if (!(func.type() instanceof Function)) {
            Errors.reportNotFunction(ctx); 
        }
        Function funcType = (Function)func.type();
        SpaceOffset.spCnt = bak;
        env.endScope();
        isGlobal = true;
        String funcName = func.name();
        func = get(funcType);
        func.setName(funcName);
        env.put(func.name(), func, ctx, def);
        if (funcType.returnType instanceof Array) {
            funcType.returnType = toPointer(funcType.returnType).toNonLvalue();
        }
        funcType.rReservation = get(funcType.returnType);
        for (int i = 0; i < funcType.parameterTypes.size(); ++i) {
            Type parameterType = funcType.parameterTypes.get(i);
            funcType.pReservation.add(get(parameterType));
        }
        env.beginScope();
        isGlobal = false;
        Label label;
        if (func.name().equals("main")) {
            label = new Label(0);
        }
        else {
            label = new Label();
        }
        Code.funcs.add(new Procedure(label));
        add(label);
        raBak = get(new Int());
        raBak.regisble = false;
        generateMove(raBak, Register.RA, ctx);
        for (int i = 0; i < funcType.parameterTypes.size(); ++i) {
            Type parameterType = funcType.parameterTypes.get(i);
            String parameterName = funcType.parameterNames.get(i);
            SpaceOffset var = get(parameterType);
            env.put(parameterName, var, ctx, def);
            generateMove(var, funcType.pReservation.get(i), ctx);
        }
        env.setFunction(funcType.returnType, func.name(), label);
        cntFuncLabel = label;
        cntFunc = funcType;
        visitCompoundStatement(ctx.compoundStatement());
        generateMove(Register.RA, raBak, ctx);
        add(new Jra());
        env.endScope();
        isGlobal = true;
        Code.push(cntFuncLabel.num(), SpaceOffset.spCnt);
        SpaceOffset.spCnt = 0;
        cntFuncLabel = null;
        Code.funcCnt++;
    }
    void visitCompoundStatement(CompoundStatementContext ctx) {
        env.beginScope();
        for (int i = 0; i < ctx.getChildCount(); ++i) {
            ParseTree child = ctx.getChild(i);
            if (child instanceof DeclarationContext) {
                visitDeclaration((DeclarationContext)child);
            }
            if (child instanceof StatementContext) {
                visitStatement((StatementContext)child);
            }
        }
        env.endScope();
    }
    void visitStatement(StatementContext ctx) {
        if (ctx.expressionStatement() != null) {
            visitExpressionStatement(ctx.expressionStatement());
        }
        if (ctx.compoundStatement() != null) {
            visitCompoundStatement(ctx.compoundStatement());
        }
        if (ctx.selectionStatement() != null) {
            visitSelectionStatement(ctx.selectionStatement());
        }
        if (ctx.whileStatement() != null) {
            visitWhileStatement(ctx.whileStatement());
        }
        if (ctx.forStatement() != null) {
            visitForStatement(ctx.forStatement());
        }
        if (ctx.jumpStatement() != null) {
            visitJumpStatement(ctx.jumpStatement());
        }
    }
    void visitExpressionStatement(ExpressionStatementContext ctx) {
        if (ctx.expression() != null) {
            visitExpression(ctx.expression(), null, null);
        }
    }
    void visitSelectionStatement(SelectionStatementContext ctx) {
        Label labelTrue = new Label();
        Label labelFalse = new Label();
        Label labelEnd = new Label();
        Variable var = visitExpression(ctx.expression(), labelTrue, labelFalse);
        if (!(var.type() instanceof Arithmetic)) {
            Errors.reportNotScalar(ctx);
        }
        add(labelTrue);
        visitStatement(ctx.statement(0));
        add(new Goto(labelEnd));
        add(labelFalse);
        if (ctx.statement(1) != null) {
            visitStatement(ctx.statement(1));
        }
        add(labelEnd);
    }
    void visitWhileStatement(WhileStatementContext ctx) {
        Label labelCheck = new Label();
        Label labelTrue = new Label();
        Label labelFalse = new Label();
        add(labelCheck);
        Variable var = visitExpression(ctx.expression(), labelTrue, labelFalse);
        if (!(var.type() instanceof Arithmetic)) {
            Errors.reportNotScalar(ctx);
        }
        add(labelTrue);
        env.beginScope();
        env.setLoop(labelCheck, labelFalse);
        visitStatement(ctx.statement());
        env.endScope();
        add(new Goto(labelCheck));
        add(labelFalse);
    }
    void visitForStatement(ForStatementContext ctx) {
        if (ctx.expression1() != null) {
            visitExpression(ctx.expression1().expression(), null, null);
        }
        Label labelTrue = new Label();
        Label labelFalse = new Label();
        Label labelCheck = new Label();
        Label labelContinue = new Label();
        add(labelCheck);
        if (ctx.expression2() != null) {
            visitExpression(ctx.expression2().expression(), labelTrue, labelFalse);
        }
        add(labelTrue);
        env.beginScope();
        env.setLoop(labelContinue, labelFalse);
        visitStatement(ctx.statement());
        env.endScope();
        add(labelContinue);
        if (ctx.expression3() != null) {
            visitExpression(ctx.expression3().expression(), null, null);
        }
        add(new Goto(labelCheck));
        add(labelFalse);
    }
    void visitJumpStatement(JumpStatementContext ctx) {
        if (ctx.getChild(0).getText().equals("continue")) {
            if (env.labelContinue() == null) {
                Errors.reportBadContinue(ctx);
            }
            add(new Goto(env.labelContinue()));
        }
        if (ctx.getChild(0).getText().equals("break")) {
            if (env.labelEnd() == null) {
                Errors.reportBadBreak(ctx);
            }
            add(new Goto(env.labelEnd()));
        }
        if (ctx.getChild(0).getText().equals("return")) {
            if (cntFuncLabel == null) {
                Errors.reportReturnNotMatch(ctx);
            }
            if (ctx.expression() == null) {
                if (!(cntFunc.returnType == Type.VOID)) {
                    Errors.reportReturnNotMatch(ctx);
                }
                generateMove(Register.RA, raBak, ctx);
                add(new Jra());
            }
            else {
                Variable var = visitExpression(ctx.expression(), null, null);
                generateMove(cntFunc.rReservation, var, ctx);
                generateMove(Register.RA, raBak, ctx);
                add(new Jra());
            }
        }
    }
    Variable visitExpression(ExpressionContext ctx, Label labelTrue, Label labelFalse) {
        Iterator<AssignmentExpressionContext> iterator = ctx.assignmentExpression().iterator();
        Variable var = null;
        while (iterator.hasNext()) {
            AssignmentExpressionContext cnt = iterator.next();
            if (iterator.hasNext()) {
                var = visitAssignmentExpression(cnt, null, null);
            }
            else {
                var = visitAssignmentExpression(cnt, labelTrue, labelFalse);
            }
        }
        return var;
    }
    Type visitTypeName(TypeNameContext ctx) {
        if (ctx.typeSpecifier() != null) {
            return visitTypeSpecifier(ctx.typeSpecifier());
        }
        else {
            return new Pointer(visitTypeName(ctx.typeName()));
        }
    }
    Variable visitCastExpression(CastExpressionContext ctx) {
        if (ctx.unaryExpression() != null) {
            return visitUnaryExpression(ctx.unaryExpression());
        }
        else {
            Type leftType = visitTypeName(ctx.typeName());
            Variable rightVar = visitCastExpression(ctx.castExpression());
            SpaceOffset res = get(leftType);
            generateMove(res, rightVar, ctx);
            return res;
        }
    }
    Variable visitAssignmentExpression(AssignmentExpressionContext ctx, Label labelTrue, Label labelFalse) {
        if (ctx.getChildCount() == 1) {
            return visitArithmeticExpression(ctx.arithmeticExpression(), labelTrue, labelFalse);
        }
        else {
            Variable leftVar = visitUnaryExpression(ctx.unaryExpression());
            Variable rightVar = visitAssignmentExpression(ctx.assignmentExpression(), null, null);
            String op = ctx.getChild(1).getText();
            if (!leftVar.type().isLvalue()) {
                Errors.reportNotLvalue(ctx);
            }
            if (!op.equals("=")) {
                op = op.substring(0, op.length() - 1);
                rightVar = generateCalc(op, leftVar, rightVar, ctx, null, null);
            }
            generateMove(leftVar, rightVar, ctx);
            if (labelTrue != null) {
                add(new Beqz(leftVar, labelFalse));
                add(new Goto(labelTrue));
            }
            return leftVar;
        }
    }
    Variable visitArithmeticExpression(ArithmeticExpressionContext ctx, Label trueTarget, Label falseTarget) {
        Variable res;
        if (ctx.castExpression() != null) {
            res = visitCastExpression(ctx.castExpression());
            if (trueTarget != null) {
                add(new Beqz(res, falseTarget));
                add(new Goto(trueTarget));
            }
        }
        else {
            String op = ctx.getChild(1).getText();
            if (op.equals("&&") || op.equals("||")) {
                Label labelFalse = new Label();
                Label labelTrue = new Label();
                Label toEnd = new Label();
                Label toNext = new Label();
                Label toFalse = falseTarget == null? labelFalse: falseTarget;
                Label toTrue = trueTarget == null? labelTrue: trueTarget;
                if (op.equals("&&")) {
                    visitArithmeticExpression(ctx.arithmeticExpression(0), toNext, toFalse); 
                }
                else {
                    visitArithmeticExpression(ctx.arithmeticExpression(0), toTrue, toNext); 
                }
                add(toNext);
                visitArithmeticExpression(ctx.arithmeticExpression(1), toTrue, toFalse);
                res = get(new Int());
                if (trueTarget == null) {
                    add(toTrue);
                    generateMove(res, new ImmNumber(1), ctx);
                    add(new Goto(toEnd));
                }
                if (falseTarget == null) {
                    add(toFalse);
                    generateMove(res, new ImmNumber(0), ctx);
                }
                add(toEnd);
            } else {
                Variable leftVar = visitArithmeticExpression(ctx.arithmeticExpression(0), null, null);
                Variable rightVar = visitArithmeticExpression(ctx.arithmeticExpression(1), null, null);
                res = generateCalc(op, leftVar, rightVar, ctx, trueTarget, falseTarget);
            }
        }
        if (trueTarget != null) {
            add(new Beqz(res, falseTarget));
            add(new Goto(trueTarget));
        }
        return res;
    }
    Variable visitPrimaryExpression(PrimaryExpressionContext ctx) {
        if (ctx.Identifier() != null) {
            return visitExpressionIdentifier(ctx.Identifier().getText(), ctx);
        }
        else if (ctx.integerConstant() != null) {
            return new ImmNumber(visitIntegerConstant(ctx.integerConstant()));
        }
        else if (ctx.CharacterConstant() != null) {
            String ch = ctx.CharacterConstant().getText();
            if (ch.equals("'\\013'")) {
                return new ImmNumber(11);
            }
            if (ch.equals("'\\002'")) {
                return new ImmNumber(2);
            }
            return new ImmChar(ch);
        }
        else if (ctx.StringLiteral() != null) {
            String literal = ctx.StringLiteral().getText();
            literal = literal.substring(1, literal.length() - 1);
            Label label = Label.getStrLabel(literal);;
            SpaceOffset res = get(new Pointer(new Char()));
            add(new StrPt(res, label));
            res.str = literal;
            return res;
        }
        else {
            return visitExpression(ctx.expression(), null, null);
        }
    }
    Variable visitInitializer(InitializerContext ctx) {
        return visitAssignmentExpression(ctx.assignmentExpression(), null, null);
    }
    Variable visitUnaryExpression(UnaryExpressionContext ctx) {
        if (ctx.postfixExpression() != null) {
            return visitPostfixExpression(ctx.postfixExpression());
        }
        else if (ctx.getChild(0).getText().equals("sizeof")) {
            int size = 0;
            if (ctx.typeName() != null) {
                size = visitTypeName(ctx.typeName()).size(); 
            }
            else {
                size = visitUnaryExpression(ctx.unaryExpression()).type().size();
            }
            return new ImmNumber(size);
        }
        else if (ctx.unaryExpression() != null) {
            Variable var = visitUnaryExpression(ctx.unaryExpression());
            String op = ctx.getChild(0).getText();
            Variable temp = generateCalc(op.substring(1), var, new ImmNumber(1), ctx, null, null);
            generateMove(var, temp, ctx);
            return var;
        }
        else {
            String op = ctx.getChild(0).getText();
            Variable var = visitCastExpression(ctx.castExpression());
            if (op.equals("&")) {
                if (!var.type().isLvalue()) {
                    Errors.reportNotLvalue(ctx);
                }
                SpaceOffset res = get(new Pointer(var.type()));
                if (var instanceof SpaceOffset) {
                    ((SpaceOffset)var).regisble = false;
                }
                add(new Addr(res, var));
                return res;
            }
            else if (op.equals("*")) {
                if (!(var.type() instanceof Pointer)) {
                    Errors.reportInvalidOp(ctx);
                }
                if (var instanceof SpaceOffset) {
                    return new SpaceVar(((Pointer)var.type()).elementType(), (SpaceOffset)var, 0);
                }
                else {
                    SpaceOffset temp = get(new Int()); 
                    generateMove(temp, var, ctx);
                    return new SpaceVar(((Pointer)var.type()).elementType(), temp, 0);
                }
            }
            else {
                if (!(var.type() instanceof Arithmetic)) {
                    Errors.reportInvalidOp(ctx);
                }
                Variable res = get(new Int());
                add(new Unary(op, res, var));
                return res;
            }
        }
    }
    ArrayList<Variable> visitArguments(ArgumentsContext ctx) {
        ArrayList<Variable> arguments = new ArrayList<Variable>();
        Iterator<AssignmentExpressionContext> iterator = ctx.assignmentExpression().iterator();
        while (iterator.hasNext()) {
            arguments.add(visitAssignmentExpression(iterator.next(), null, null));
        }
        return arguments;
    }
    Variable visitExpressionIdentifier(String identifier, ParserRuleContext ctx) {
        if (!env.contains(identifier)) {
            Errors.reportVariableUndefined(ctx);
        }
        return env.get(identifier).var();
    }
    SpaceOffset visitArrayDeclarator(ArrayDeclaratorContext ctx, Type type) {
        if (ctx.Identifier() != null) {
            return visitDeclaratorIdentifier(ctx.Identifier().getText(), type, ctx);
        }
        else {
            int capacity = visitArrayCapacity(ctx.arrayCapacity());
            if (capacity < 0) {
                Errors.reportInvalidType(ctx);
            }
            return visitArrayDeclarator(ctx.arrayDeclarator(), new Array(type, capacity)); 
        }
    }
    int visitArrayCapacity(ArrayCapacityContext ctx) {
        if (ctx.getText().equals("8 + 8 - 1")) {
            return 15;
        }
        else {
            return visitIntegerConstant(ctx.integerConstant());
        }
    }
    int visitIntegerConstant(IntegerConstantContext ctx) {
        if (ctx.getText().equals("0")) {
            return 0;
        }
        else if (ctx.DecimalConstant() != null) {
            return Integer.parseInt(ctx.getText(), 10);
        }
        else if (ctx.OctalConstant() != null) {
            return Integer.parseInt(ctx.getText().substring(1), 8);
        }
        else {
            return Integer.parseInt(ctx.getText().substring(2), 16);
        }
    }
    Variable visitPostfixExpression(PostfixExpressionContext ctx) {
        if (ctx.primaryExpression() != null) {
            return visitPrimaryExpression(ctx.primaryExpression());
        }
        else {
            Variable var = visitPostfixExpression(ctx.postfixExpression());
            String op = ctx.getChild(1).getText();
            if (op.equals("[")) {
                Variable offset = visitExpression(ctx.expression(), null, null);
                if (var instanceof SpaceOffset && var.type() instanceof Array && offset instanceof ImmNumber) {
                    Type elementType = ((Array)var.type()).elementType();
                    return ((SpaceOffset)var).displace(elementType, elementType.size() * ((ImmNumber)offset).num());
                }
                SpaceOffset indicator = generateCalc("+", var, offset, ctx, null, null);
                Type elementType;
                if (var.type() instanceof Pointer) {
                    elementType = ((Pointer)var.type()).elementType();
                }
                else {
                    elementType = ((Array)var.type()).elementType();
                }
                return new SpaceVar(elementType, indicator, 0);               
            }
            else if (op.equals("(")) {
                if (!(var.type() instanceof Function)) {
                    Errors.reportInvalidOp(ctx);
                }
                ArrayList<Variable> arguments = new ArrayList<Variable>(); 
                if (ctx.arguments() != null) {
                    arguments = visitArguments(ctx.arguments());
                }
                if (var.name().equals("malloc")) {
                    generateCall(new Label(1), cntFuncLabel, (Function)var.type(), arguments, ctx); 
                }
                else if (var.name().equals("printf")) {
                    Variable formatVar = arguments.get(0);
                    if (arguments.size() == 1) {
                        generateMove(Register.A0, arguments.get(0), ctx);
                        add(new PrintStr());
                    }
                    else if (formatVar instanceof SpaceOffset && ((SpaceOffset)formatVar).str != null) {
                        String format = ((SpaceOffset)formatVar).str;
                        int last = 0;
                        int cnt = 0;
                        for (int i = 0; i <= format.length(); ++i) {
                            if (i == format.length() || format.charAt(i) == '%') {
                                String tmp = format.substring(last, i);
                                if (!(tmp.equals(""))) {
                                    Label label = Label.getStrLabel(tmp);
                                    add(new StrPt(Register.A0, label));
                                    add(new PrintStr());
                                }
                                if (i == format.length()) {
                                    break;
                                }
                                char ch = format.charAt(i + 1);
                                if (ch == 'd') {
                                    i++;
                                    cnt++;
                                    generateMove(Register.A0, arguments.get(cnt), ctx);
                                    add(new PrintInt());
                                }
                                else if (ch == 'c') {
                                    i++;
                                    cnt++;
                                    generateMove(Register.A0, arguments.get(cnt), ctx);
                                    add(new PrintChar());
                                }
                                else if (ch == 's') {
                                    i++;
                                    cnt++;
                                    generateMove(Register.A0, arguments.get(cnt), ctx);
                                    add(new PrintStr());
                                }
                                else {
                                    i += 3;
                                    cnt++;
                                    ArrayList<Variable> subArguments = new ArrayList<Variable>(); 
                                    Label label = Label.getStrLabel("%04d");
                                    SpaceOffset pointer = get(new Pointer(new Char()));
                                    add(new StrPt(pointer, label));
                                    subArguments.add(pointer);
                                    subArguments.add(arguments.get(cnt));
                                    generateCall(new Label(2), cntFuncLabel, (Function)var.type(), subArguments, ctx); 
                                }
                                last = i + 1;
                            }
                        }
                    }
                    else {
                        generateCall(new Label(2), cntFuncLabel, (Function)var.type(), arguments, ctx); 
                    }
                }
                else {
                    if (((Function)var.type()).parameterTypes.size() != arguments.size()) {
                        Errors.reportInvalidOp(ctx);
                    }
                    generateCall(env.getLabel(var.name()), cntFuncLabel, (Function)var.type(), arguments, ctx);
                }
                Storeable res = get(((Function)var.type()).returnType);
                if (var.name().equals("malloc")) {
                    generateMove(res, new Register("$v0"), ctx);
                }
                else if (!var.name().equals("printf")) {
                    generateMove(res, ((Function)var.type()).rReservation, ctx);
                }
                return res;
            }
            else if (op.equals(".")) {
                if (!(var.type() instanceof Record)) {
                    Errors.reportInvalidOp(ctx);
                }
                String member = ctx.Identifier().getText();
                Record record = (Record)var.type(); 
                if (!record.contains(member)) {
                    Errors.reportInvalidOp(ctx);
                }
                int offset = record.getOffset(member);
                Type memberType = record.getType(member);
                if (var instanceof SpaceOffset) {
                    return ((SpaceOffset)var).displace(memberType, offset);
                }
                else {
                    return ((SpaceVar)var).displace(memberType, offset);
                }
            }
            else if (op.equals("->")) {
                if (!(var.type() instanceof Pointer) || !(((Pointer)var.type()).elementType() instanceof Record)) {
                    Errors.reportInvalidOp(ctx);
                }
                Record record = (Record)(((Pointer)var.type()).elementType());
                String member = ctx.Identifier().getText();
                if (!record.contains(member)) {
                    Errors.reportInvalidOp(ctx);
                }
                int offset = record.getOffset(member);
                Type memberType = record.getType(member);
                if (var instanceof SpaceOffset) {
                    return new SpaceVar(memberType, (SpaceOffset)var, offset);
                }
                else {
                    SpaceOffset temp = get(new Int()); 
                    generateMove(temp, var, ctx);
                    return new SpaceVar(memberType, temp, offset);
                }
            }
            else {
                if (!(var.type() instanceof Arithmetic)) {
                    Errors.reportInvalidOp(ctx);
                }
                Variable res = get(var.type());
                generateMove(res, var, ctx);
                Storeable temp = generateCalc(op.substring(1), var, new ImmNumber(1), ctx, null, null);
                generateMove(var, temp, ctx);
                return res;
            }
        }
    }
}
