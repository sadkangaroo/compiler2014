package compiler.writer;

import java.util.*;

import compiler.code.*;
import compiler.optimizer.*;

public class Writer {
    public static ArrayList<String> data = new ArrayList<String>();
    public static  ArrayList<String> text = new ArrayList<String>();
    public static String libPath;
    public static void write() {
        Code.funcs.add(new Procedure(new Label(-1)));
        Procedure cnt = Code.funcs.get(Code.funcCnt);
        cnt.add(new Label(-1));
        cnt.add(new SetGp());
        for (int i = 0; i < Code.global.size(); ++i) {
            cnt.add(Code.global.get(i));
        }
        cnt.add(new Jal(new Label(0)));
        cnt.add(new Exit());
        Code.funcCnt++;
        ArrayList<Tac> program = Processor.expand(Code.funcs);
        Analyzer analyzer = new Analyzer(program);
        analyzer.analyze();
        for (int i = 0; i < program.size(); ++i) {
            program.get(i).write();
        }
        data.add(".align 4");
        data.add(".space 32768");
        data.add("GLOBAL:");
        output();
    }
    public static void output() {
        System.out.println(".text");
        for (int i = 0; i < text.size(); ++i) {
            System.out.println(text.get(i));
        }
        System.out.println("\n.data 0x10000000\n");
        for (int i = 0; i < data.size(); ++i) {
            System.out.println(data.get(i));
        }
    }
    public static void addData(Label label, String str) {
        data.add(label.toString() + ": " + ".asciiz " + "\"" + str + "\"");
    }
}
