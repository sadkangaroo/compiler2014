package compiler.main;

import compiler.syntactic.*;
import compiler.semantic.*;
import compiler.errors.*;

import java.io.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class Main {
    static void compile(String filename, String libPath) throws IOException {
        ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(filename));
        GrammarLexer lexer = new GrammarLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        GrammarParser parser = new GrammarParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(Errors.listener());
        ParseTree tree = parser.compilationUnit();
        Translator translator = new Translator(libPath);
        translator.translate(tree);
    }
    public static void main(String[] args) throws IOException {
        boolean isLocalTest = false; 
        String userDir = System.getProperty("user.dir") + "/";
        if (isLocalTest) {
            compile("/home/sadkangaroo/tryspace/data.c", "/home/sadkangaroo/compiler2014/lib/");
        }
        else {
            for (String arg: args){
                compile(userDir + arg, userDir + "../lib/");
            }
        }
    }
}
