package compiler.errors;

import org.antlr.v4.runtime.*;

class ErrorListener extends BaseErrorListener {
    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        System.out.println("line " + line + ":" + charPositionInLine + ": " + msg);
        System.exit(1);
    }
}

public class Errors {
    static final ErrorListener listener = new ErrorListener();
    public static ErrorListener listener() {
        return listener;
    }
    static void printErrorMessage(ParserRuleContext ctx, String msg) {
        Token firstToken = ctx.getStart();
        int line = firstToken.getLine();
        int col = firstToken.getCharPositionInLine();
        System.out.println("line " + line + ":" + col + ": " + msg);
        System.exit(1);
    }
    static public void reportIncompatible(ParserRuleContext ctx) {
        printErrorMessage(ctx, "incompatible types when assigning");
    }
    static public void reportConflict(ParserRuleContext ctx) {
        printErrorMessage(ctx, "conflicting types for the same variable");
    }
    static public void reportRedefinition(ParserRuleContext ctx) {
        printErrorMessage(ctx, "redefinition of the same variable");
    }
    static public void reportNotLvalue(ParserRuleContext ctx) {
        printErrorMessage(ctx, "lvalue required as left operand of assignment");
    }
    static public void reportInvalidType(ParserRuleContext ctx) {
        printErrorMessage(ctx, "variable has invalid type");
    }
    static public void reportNotFunction(ParserRuleContext ctx) {
        printErrorMessage(ctx, "not a valid function definition");
    }
    static public void reportReturnNotMatch(ParserRuleContext ctx) {
        printErrorMessage(ctx, "return type not match");
    }
    static public void reportBadContinue(ParserRuleContext ctx) {
        printErrorMessage(ctx, "continue not in a loop");
    }
    static public void reportBadBreak(ParserRuleContext ctx) {
        printErrorMessage(ctx, "break not in a loop");
    }
    static public void reportInvalidOp(ParserRuleContext ctx) {
        printErrorMessage(ctx, "invalid operation");
    }
    static public void reportVariableUndefined(ParserRuleContext ctx) {
        printErrorMessage(ctx, "variable undefined");
    }
    static public void reportIncompleteParameter(ParserRuleContext ctx) {
        printErrorMessage(ctx, "incomplete parameter");
    }
    static public void reportNotScalar(ParserRuleContext ctx) {
        printErrorMessage(ctx, "variable is not scalar");
    }
    static public void reportTypeNotDefined(ParserRuleContext ctx) {
        printErrorMessage(ctx, "type not defined");
    }
}
