package compiler.code;

import compiler.variable.*;
import compiler.writer.*;

import java.util.*;

public final class Label extends Tac {
    private static int count = 3;
    private static HashMap<String, Label> str = new HashMap<String, Label>();
    public String renamer;
    int num;
    public Label() {
        num = count++;
        renamer = "";
    }
    public Label(int num) {
        this.num = num;
        renamer = "";
    }
    public Label(int num, String renamer) {
        this.num = num;
        this.renamer = renamer;
    }
    public int num() {
        return num;
    }
    public static Label getStrLabel(String literal) {
        Label res;
        if (str.containsKey(literal)) {
            res = str.get(literal);
        }
        else {
            res = new Label();
            str.put(literal, res);
            Writer.addData(res, literal);
        }
        return res;
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public String toString() {
        if (num == -1) {
            return "main";
        }
        else {
            return "Label" + String.valueOf(num) + renamer;
        }
    }
    @Override
    public void write() {
        Code.emit("\n%s:", toString());
    }
    @Override 
    public Label relabel(String renamer) {
        return new Label(num, this.renamer + "_" + renamer);
    }
    @Override 
    public boolean equals(Object object) {
        return object instanceof Label && num == ((Label)object).num && renamer.equals(((Label)object).renamer);
    }
    @Override
    public int hashCode() {
        return num * 999983 + renamer.hashCode();
    }
}
