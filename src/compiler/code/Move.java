package compiler.code;

import compiler.variable.*;
import compiler.optimizer.*;

import java.util.*;

public class Move extends Tac {
    Variable des, src;
    public Move(Variable des, Variable src) {
        this.des = des;
        this.src = src;
    }
    public boolean withRA() {
        return des == Register.RA || src == Register.RA;
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        res.add(src);
        if (des instanceof SpaceVar) {
            res.add(((SpaceVar)des).indicator());
        }
        if (src instanceof SpaceVar) {
            res.add(((SpaceVar)src).indicator());
        }
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        res.add(des);
        return res;
    }
    @Override
    public void write() {
        if (Analyzer.dead(this, des)) {
            return;
        }
        des = des.allocate();
        src = src.allocate();
        if (des instanceof Register && src instanceof Register) {
            if (!des.name().equals(src.name())) {
                Code.emit("move %s, %s", des.name(), src.name());
            }
        }
        else if (des instanceof Register) {
            Code.load(des.name(), src);
        }
        else if (src instanceof Register) {
            Code.store(src.name(), des);
        }
        else {
            Code.load("$a1", src);
            Code.store("$a1", des);
        }
    }
    @Override 
    public Tac relabel(String renamer) {
        return new Move(des, src);
    }
}
