package compiler.code;

import compiler.variable.*;
import compiler.optimizer.*;

import java.util.*;

public final class StrPt extends Tac {
    Variable des;
    Label label;
    public StrPt(Variable des, Label label) {
        this.des = des;
        this.label = label;
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        if (des instanceof SpaceVar) {
            res.add(((SpaceVar)des).indicator());
        }
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        res.add(des);
        return res;
    }
    @Override
    public void write() {
        if (Analyzer.dead(this, des)) {
            return;
        }
        des = des.allocate();
        String temp = des.fall("$a1");
        Code.emit("la %s, %s", temp, label.toString());
        Code.store(temp, des);
    }
    @Override 
    public Tac relabel(String renamer) {
        return new StrPt(des, label);
    }
}
