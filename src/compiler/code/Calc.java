package compiler.code;

import compiler.variable.*;
import compiler.optimizer.*;
import compiler.semantic.*;

import java.util.*;

public final class Calc extends Tac {
    String op;
    Variable des;
    Variable src1, src2;
    HashMap<String, String> cmd;
    HashMap<String, String> jump;
    Label trueTarget;
    Label falseTarget;
    public Calc(String op, Variable des, Variable src1, Variable src2, Label trueTarget, Label falseTarget) {
        this.op = op;
        this.des = des;
        this.src1 = src1;
        this.src2 = src2;
        this.trueTarget = trueTarget;
        this.falseTarget = falseTarget;
        this.falseTarget = falseTarget;
        cmd = new HashMap<String, String>();
        jump = new HashMap<String, String>();
        cmd.put("+", "addu");
        cmd.put("-", "subu");
        cmd.put("*", "mul");
        cmd.put("/", "divu");
        cmd.put("%", "rem");
        cmd.put("<<", "sll");
        cmd.put(">>", "srl");
        cmd.put("|", "or");
        cmd.put("&", "and");
        cmd.put("^", "xor");
        cmd.put(">", "sgt");
        cmd.put(">=", "sge");
        cmd.put("<", "slt");
        cmd.put("<=", "sle");
        cmd.put("==", "seq");
        cmd.put("!=", "sne");
        jump.put("==", "beq");
        jump.put("!=", "bne");
        jump.put(">", "bgt");
        jump.put(">=", "bge");
        jump.put("<", "blt");
        jump.put("<=", "ble");
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        res.add(src1);
        res.add(src2);
        if (des instanceof SpaceVar) {
            res.add(((SpaceVar)des).indicator());
        }
        if (src1 instanceof SpaceVar) {
            res.add(((SpaceVar)src1).indicator());
        }
        if (src2 instanceof SpaceVar) {
            res.add(((SpaceVar)src2).indicator());
        }
        return res;
    }
    void generate(String op, String temp1, String temp2, Variable src2, Label trueTarget, Label falseTarget) {
        if (src2 instanceof ImmNumber) {
            Code.emit("%s %s, %s, %s", op, temp1, ((ImmNumber)src2).num(), trueTarget.toString());
        }
        else {
            Code.load(temp2, src2);
            Code.emit("%s %s, %s, %s", op, temp1, temp2, trueTarget.toString());
        }
        Code.emit("b %s", falseTarget.toString());
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        res.add(des);
        return res;
    }
    @Override
    public void write() {
        if (Analyzer.dead(this, des)) {
            return;
        }
        src1 = src1.allocate();
        String temp1 = src1.fall("$a1");
        src2 = src2.allocate();
        String temp2 = src2.fall("$a2");
        des = des.allocate();
        String temp3 = des.fall("$a1");
        Code.load(temp1, src1);
        if (trueTarget != null && Translator.isComp(op)) {
            generate(jump.get(op), temp1, temp2, src2, trueTarget, falseTarget);
        }
        else {
            if (src2 instanceof ImmNumber) {
                Code.emit("%s %s, %s, %s", cmd.get(op), temp3, temp1, ((ImmNumber)src2).num());
            }
            else {
                Code.load(temp2, src2);
                Code.emit("%s %s, %s, %s", cmd.get(op), temp3, temp1, temp2);
            }
            Code.store(temp3, des);
        }
    }
    @Override 
    public Tac relabel(String renamer) {
        return new Calc(op, des, src1, src2, trueTarget == null? null: trueTarget.relabel(renamer), falseTarget == null? null: falseTarget.relabel(renamer));
    }
}
