package compiler.code;

import compiler.variable.*;

import java.util.*;

public final class Goto extends Jump {
    Label label;
    public Goto(Label label) {
        this.label = label;
    }
    public Label label() {
        return label;
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public void write() {
        Code.emit("b %s", label.toString());
    }
    public Tac relabel(String renamer) {
        return new Goto(label.relabel(renamer));
    }
}
