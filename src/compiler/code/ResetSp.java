package compiler.code;

import compiler.variable.*;
import compiler.optimizer.*;

import java.util.*;

public final class ResetSp extends Tac {
    int labNum;
    public boolean save;
    public ResetSp(int labNum, boolean save) {
        this.labNum = labNum;
        this.save = save;
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public void write() {
        Code.emit("addu $sp, $sp, %d", -Code.stackOffset.get(labNum));
        if (save) {
            BitSet cnt = Analyzer.liveIn.get(this);  
            for (int k = 0; k < SpaceOffset.spId; ++k) {
                if (cnt.get(k)) {
                    Variable var = SpaceOffset.localVar.get(k);
                    if (Analyzer.regs[k] != null) {
                        Code.load(Analyzer.regs[k], var);
                    }
                }
            }
        }
    }
    @Override 
    public Tac relabel(String renamer) {
        return new ResetSp(labNum, save);
    }
}
