package compiler.code;

import compiler.variable.*;

import java.util.*;

public final class PrintStr extends Tac {
    public PrintStr() {
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public void write() {
        Code.emit("li $v0, 4");
        Code.emit("syscall");
    }
    @Override 
    public Tac relabel(String renamer) {
        return new PrintStr();
    }
}
