package compiler.code;

import compiler.variable.*;

import java.util.*;

public final class Jal extends Tac {
    Label label;
    public Jal(Label label) {
        this.label = label;
    }
    public Label label() {
        return label;
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public void write() {
        Code.emit("jal %s", label.toString());
    }
    @Override 
    public Tac relabel(String renamer) {
        return new Jal(label);
    }
}
