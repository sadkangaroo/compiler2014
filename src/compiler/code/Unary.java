package compiler.code;

import compiler.variable.*;
import compiler.optimizer.*;

import java.util.*;

public final class Unary extends Tac {
    String op;
    Variable des, src;
    public Unary(String op, Variable des, Variable src) {
        this.op = op;
        this.des = des;
        this.src = src;
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        res.add(src);
        if (des instanceof SpaceVar) {
            res.add(((SpaceVar)des).indicator());
        }
        if (src instanceof SpaceVar) {
            res.add(((SpaceVar)src).indicator());
        }
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        res.add(des);
        return res;
    }
    @Override
    public void write() {
        if (Analyzer.dead(this, des)) {
            return;
        }
        des = des.allocate();
        String temp1 = des.fall("$a1");
        src = src.allocate();
        String temp2 = src.fall("$a2");
        Code.load(temp2, src);
        if (op.equals("-")) {
            Code.emit("neg %s, %s", temp1, temp2);
        }
        if (op.equals("~")) {
            Code.emit("not %s, %s", temp1, temp2);
        }
        if (op.equals("!")) {
            Code.emit("seq %s, %s, 0", temp1, temp2);
        }
        Code.store(temp1, des);
    }
    @Override 
    public Tac relabel(String renamer) {
        return new Unary(op, des, src);
    }
}
