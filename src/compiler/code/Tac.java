package compiler.code;

import compiler.variable.*;

import java.util.*;

public abstract class Tac {
    public abstract void write();
    public abstract ArrayList<Variable> def();
    public abstract ArrayList<Variable> ref();
    public abstract Tac relabel(String renamer);
}

