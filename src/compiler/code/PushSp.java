package compiler.code;

import compiler.variable.*;
import compiler.optimizer.*;

import java.util.*;

public final class PushSp extends Tac {
    int labNum;
    public boolean save;
    public PushSp(int labNum, boolean save) {
        this.labNum = labNum;
        this.save = save;
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public void write() {
        if (save) {
            BitSet cnt = Analyzer.liveIn.get(this);  
            for (int k = 0; k < SpaceOffset.spId; ++k) {
                if (cnt.get(k)) {
                    Variable var = SpaceOffset.localVar.get(k);
                    if (Analyzer.regs[k] != null) {
                        Code.store(Analyzer.regs[k], var);
                    }
                }
            }
        }
        Code.emit("addu $sp, $sp, %d", Code.stackOffset.get(labNum));
    }
    @Override 
    public Tac relabel(String renamer) {
        return new PushSp(labNum, save);
    }
}
