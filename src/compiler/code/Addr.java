package compiler.code;

import java.util.*;

import compiler.variable.*;
import compiler.optimizer.*;

public final class Addr extends Tac {
    Variable des;
    Variable src;
    public Addr(Variable des, Variable src) {
        this.des = des;
        this.src = src;
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        if (des instanceof SpaceVar) {
            res.add(((SpaceVar)des).indicator());
        }
        if (src instanceof SpaceVar) {
            res.add(((SpaceVar)src).indicator());
        }
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        res.add(des);
        return res;
    }
    @Override
    public void write() {
        if (Analyzer.dead(this, des)) {
            return;
        }
        des = des.allocate();
        src = src.allocate();
        String temp = des.fall("$a1");
        if (src instanceof SpaceOffset) {
            Code.emit("la %s, %s", temp, src.toString());
        }
        else {
            Code.load(temp, ((SpaceVar)src).indicator());
            int offset = ((SpaceVar)src).offset();
            if (offset != 0) {
                Code.emit("addu %s, %s, %d", temp, temp, offset);
            }
        }
        Code.store(temp, des);
    }
    @Override 
    public Tac relabel(String renamer) {
        return new Addr(des, src);
    }
}
