package compiler.code;

import compiler.variable.*;

import java.util.*;

public final class Bnez extends Jump {
    Label label;
    Variable src; 
    public Bnez(Variable src, Label label) {
        this.src = src;
        this.label = label;
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        res.add(src);
        if (src instanceof SpaceVar) {
            res.add(((SpaceVar)src).indicator());
        }
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    public Label label() {
        return label;
    }
    @Override
    public void write() {
        src = src.allocate();
        String temp = src.fall("$a1");
        Code.load(temp, src);
        Code.emit("bnez %s, %s", temp, label.toString());
    }
    @Override 
    public Tac relabel(String renamer) {
        return new Bnez(src, label.relabel(renamer));
    }
}
