package compiler.code;

import compiler.variable.*;

import java.util.*;

public final class Jra extends Jump {
    public Jra() {
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public void write() {
        Code.emit("jr $ra");
    }
    @Override 
    public Tac relabel(String renamer) {
        return new Jra();
    }
}
