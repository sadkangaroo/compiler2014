package compiler.code;

import java.util.*;

import compiler.variable.*;
import compiler.optimizer.*;
import compiler.writer.*;

public class Code {
    public static int funcCnt = 0;
    static HashMap<Integer, Integer> stackOffset = new HashMap<Integer, Integer>();
    public static ArrayList<Tac> global = new ArrayList<Tac>();
    public static ArrayList<Procedure> funcs = new ArrayList<Procedure>();
	public static void emit(String fmt, Object... objects) {
		Writer.text.add(String.format(fmt, objects));
	}
	public static void emitRaw(String str, int cnt) {
        str = str.replaceAll("printf", "printf" + String.valueOf(cnt));
		Writer.text.add(str);
	}
    public static void put(Tac sentence, boolean isGlobal) {
        if (isGlobal) {
            global.add(sentence);        
        }
        else {
            funcs.get(funcCnt).add(sentence);
        }
    }
    public static void push(int label, int offset) {
        stackOffset.put(label, offset);
    }
    public static void load(String reg, Variable var) {
        if (var instanceof SpaceOffset) {
            Code.emit("lw %s, %s", reg, var.toString());
        }
        if (var instanceof SpaceVar) {
            Variable indicator = ((SpaceVar)var).indicator();
            String temp = indicator.fall("$a1");
            load(temp, indicator);
            Code.emit("lw %s, %d(%s)", reg, ((SpaceVar)var).offset(), temp);
        }
        if (var instanceof ImmNumber) {
            Code.emit("li %s, %d", reg, ((ImmNumber)var).num());
        }
        if (var instanceof ImmChar) {
            Code.emit("li %s, %s", reg, ((ImmChar)var).ch());
        }
        if (var instanceof Register) {
            if (!((Register)var).name().equals(reg)) {
                Code.emit("move %s, %s", reg, ((Register)var).name());
            }
        }
    }
    public static void store(String reg, Variable var) {
        if (var instanceof SpaceOffset) {
            Code.emit("sw %s, %s", reg, var.toString());
        }
        if (var instanceof Register) {
            if (!((Register)var).name().equals(reg)) {
                Code.emit("move %s, %s", ((Register)var).name(), reg);
            }
        }
        if (var instanceof SpaceVar) {
            Variable indicator = ((SpaceVar)var).indicator();
            String target = "$a1";
            if (reg.equals("$a1")) {
                target = "$a2";
            }
            String temp = indicator.fall(target);
            load(temp, indicator);
            Code.emit("sw %s, %d(%s)", reg, ((SpaceVar)var).offset(), temp);
        }
    }
}
