package compiler.code;

import compiler.variable.*;

import java.util.*;
import java.io.*;

public final class Printf extends Tac {
    public static int cnt = 0;
    public Printf() {
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public void write() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(compiler.writer.Writer.libPath + "printf.s"));
            String line;
            while ((line = reader.readLine()) != null) {
                Code.emitRaw(line, cnt);
            }
            reader.close();
        } catch (IOException e) {
            System.out.println("Cannot load lib");
        }
        cnt++;
    }
    @Override 
    public Tac relabel(String renamer) {
        return new Printf();
    }
}
