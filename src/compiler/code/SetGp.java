package compiler.code;

import compiler.variable.*;

import java.util.*;

public final class SetGp extends Tac {
    public SetGp() {
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    @Override
    public void write() {
        Code.emit("la $gp, GLOBAL");
    }
    @Override 
    public Tac relabel(String renamer) {
        return new SetGp();
    }
}
