package compiler.code;

import compiler.variable.*;

import java.util.*;

public final class Beqz extends Jump {
    Label label;
    Variable src; 
    public Beqz(Variable src, Label label) {
        this.src = src;
        this.label = label;
    }
    @Override
    public ArrayList<Variable> ref() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        res.add(src);
        if (src instanceof SpaceVar) {
            res.add(((SpaceVar)src).indicator());
        }
        return res;
    }
    @Override
    public ArrayList<Variable> def() {
        ArrayList<Variable> res = new ArrayList<Variable>();
        return res;
    }
    public Label label() {
        return label;
    }
    @Override
    public void write() {
        src = src.allocate();
        String temp = src.fall("$a1");
        Code.load(temp, src);
        Code.emit("beqz %s, %s", temp, label.toString());
    }
    @Override 
    public Tac relabel(String renamer) {
        return new Beqz(src, label.relabel(renamer));
    }
}
