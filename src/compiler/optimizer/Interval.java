package compiler.optimizer;

public class Interval implements Comparable<Interval> {
    int id;
    int start, finish;
    String reg;
    public Interval(int id, int start, int finish) {
        this.id = id;
        this.start = start;
        this.finish = finish;
        reg = null;
    }
    @Override 
    public int compareTo(Interval o) {
        if (finish != o.finish) { 
            return finish - o.finish;
        }
        else {
            return id - o.id;
        }
    }
}
