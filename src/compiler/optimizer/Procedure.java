package compiler.optimizer;

import compiler.code.*;

import java.util.*;

public class Procedure {
    ArrayList<Tac> ins;
    Label label;
    public Procedure(Label label) {
        ins = new ArrayList<Tac>();
        this.label = label;
    }
    public void add(Tac sentence) {
        ins.add(sentence);
    }
}
