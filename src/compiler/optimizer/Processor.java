package compiler.optimizer;

import compiler.code.*;

import java.util.*;

public class Processor {
    static int renameCnt;
    public Processor() {
    }
    static boolean isTerminal(Procedure procedure) {
        if (procedure.label.num() == -1) {
            return false;
        } 
        for (int i = 0; i < procedure.ins.size(); ++i) {
            if (procedure.ins.get(i) instanceof Jal) {
                return false;
            }
        }
        return true;
    }
    static void link(Procedure newProcedure, Procedure child, Label returnPoint) {
        for (int i = 0; i + 1 < child.ins.size(); ++i) {
            Tac cnt = child.ins.get(i);
            if (cnt instanceof Jra) {
                newProcedure.add(new Goto(returnPoint));
            }
            else if (!(cnt instanceof Move && ((Move)cnt).withRA())) {
                newProcedure.add(cnt.relabel(String.valueOf(renameCnt)));
            }
        }
        renameCnt++;
    }
    static Procedure embed(Procedure father, Procedure child) {
        Procedure newProcedure = new Procedure(father.label);
        for (int i = 0; i < father.ins.size(); ++i) {
            Tac cnt = father.ins.get(i);
            if (cnt instanceof Jal && ((Jal)cnt).label().equals(child.label)) {
                if (i - 1 >= 0 && father.ins.get(i - 1) instanceof PushSp) {
                    ((PushSp)father.ins.get(i - 1)).save = false;
                }
                if (i + 1 < father.ins.size() && father.ins.get(i + 1) instanceof ResetSp) {
                    ((ResetSp)father.ins.get(i + 1)).save = false;
                }
                Label returnPoint = new Label();
                link(newProcedure, child, returnPoint);
                newProcedure.ins.add(returnPoint);
            }
            else {
                newProcedure.ins.add(cnt);
            }
        }
        return newProcedure;
    }
    static void embed(ArrayList<Procedure> funcs, Procedure cnt) {
        renameCnt = 0;
        for (int i = 0; i < funcs.size(); ++i) {
            funcs.set(i, embed(funcs.get(i), cnt));
        }
    }
    public static ArrayList<Tac> expand(ArrayList<Procedure> funcs) {
        boolean hasTerminal = true;
        while (hasTerminal) {
            hasTerminal = false;
            Procedure cnt = null;
            for (int i = 0; i < funcs.size(); ++i) {
                cnt = funcs.get(i);
                if (isTerminal(cnt)) {
                    hasTerminal = true;
                    break;
                }
            }
            if (hasTerminal) {
                funcs.remove(cnt);  
                embed(funcs, cnt); 
            }
        }
        ArrayList<Tac> program = new ArrayList<Tac>();
        for (int i = 0; i < funcs.size(); ++i) {
            Procedure cnt = funcs.get(i);
            for (int k = 0; k < cnt.ins.size(); ++k) {
                program.add(cnt.ins.get(k));
            }
        }
        return program;
    }
}
