package compiler.optimizer;

import compiler.code.*;

import java.util.*;

public class BBlock {
    int insNum;
    ArrayList<Tac> ins; 
    ArrayList<BBlock> next;
    BitSet in, out;
    boolean isStart;
    boolean visited;
    public BBlock() {
        insNum = 0;
        ins = new ArrayList<Tac>();
        next = new ArrayList<BBlock>();
        in = new BitSet();
        out = new BitSet(); 
        isStart = true;
        visited = false;
    }
    public void addMember(Tac tac) {
        insNum++;
        ins.add(tac);
    }
    public void addChild(BBlock bblock) {
        next.add(bblock);
        bblock.isStart = false;
    }
    public Tac last() {
        return ins.get(insNum - 1);
    }
}
