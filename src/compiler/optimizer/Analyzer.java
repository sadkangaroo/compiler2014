package compiler.optimizer;

import compiler.code.*;
import compiler.variable.*;

import java.util.*;

public class Analyzer {
    ArrayList<Tac> program;
    ArrayList<BBlock> bblocks;
    BitSet[] status;
    int totId, totIns, cntPos;
    HashMap<Tac, Tac> nextIns;
    public static HashMap<Variable, Register> globalReg;
    public static TreeSet<String> remains;
    public static String[] regs;
    public static HashMap<Tac, BitSet> liveIn = new HashMap<Tac, BitSet>();
    public static HashMap<Tac, BitSet> liveOut = new HashMap<Tac, BitSet>();
    public Analyzer(ArrayList<Tac> program) {
        this.program = program;
        totIns = program.size();
        totId = SpaceOffset.spId;
        regs = new String[totId];
        status = new BitSet[totIns];
        bblocks = new ArrayList<BBlock>();
        globalReg = new HashMap<Variable, Register>();
    }
    public static boolean dead(Tac sentence, Variable var) {
        return var.localRegisble() && liveOut.get(sentence).get(((SpaceOffset)var).id()) == false;
    }
    public void analyze() {
        calcBlock();
        calcEquation();
        LinearScan();
    }
    void calcBlock() {
        nextIns = new HashMap<Tac, Tac>();
        HashMap<Tac, BBlock> inBlock = new HashMap<Tac, BBlock>();
        boolean[] isLeader = new boolean[totIns];
        for (int i = 0; i < totIns; ++i) {
            isLeader[i] = false;
        }
        for (int i = 0; i < totIns; ++i) {
            Tac now = program.get(i);
            if (i + 1 < totIns) {
                nextIns.put(now, program.get(i + 1));
            }
            if (now instanceof Label) {
                isLeader[i] = true;
            }
            if (now instanceof Jump && i + 1 < totIns) {
                isLeader[i + 1] = true;
            }
        }
        BBlock cntBlock = null;
        for (int i = 0; i < totIns; ++i) {
            if (isLeader[i]) {
                if (cntBlock != null) {
                    bblocks.add(cntBlock);
                }
                cntBlock = new BBlock();
            }
            cntBlock.addMember(program.get(i));
            inBlock.put(program.get(i), cntBlock);
        }
        bblocks.add(cntBlock);
        for (int i = 0; i < bblocks.size(); ++i) {
            cntBlock = bblocks.get(i);
            Tac last = cntBlock.last();
            if (last instanceof Goto) {
                cntBlock.addChild(inBlock.get(((Goto)last).label()));
            }
            else {
                if (last instanceof Beqz) {
                    cntBlock.addChild(inBlock.get(((Beqz)last).label()));
                }
                if (last instanceof Bnez) {
                    cntBlock.addChild(inBlock.get(((Bnez)last).label()));
                }
                if (!(last instanceof Exit) && !(last instanceof Jra) && nextIns.containsKey(last)) {
                    cntBlock.addChild(inBlock.get(nextIns.get(last)));
                }
            }
        }
    }
    void calcEquation() {
        boolean changed = true;
        while (changed) {
            changed = false;
            BBlock cntBlock;
            for (int k = 0; k < bblocks.size(); ++k) {
                cntBlock = bblocks.get(k);
                cntBlock.out.clear();
                for (int i = 0; i < cntBlock.next.size(); ++i) {
                    cntBlock.out.or(cntBlock.next.get(i).in);
                }
                BitSet old = (BitSet)cntBlock.in.clone();
                calcIn(cntBlock);
                if (!cntBlock.in.equals(old)) {
                    changed = true;
                }
            }
        }
    }
    void set(BitSet now, Variable var, boolean value) {
         if (var.localRegisble()) {
             now.set(((SpaceOffset)var).id(), value);
         }
    }
    void calcIn(BBlock now) {
        now.in.clear();
        now.in.or(now.out);
        for (int i = now.insNum - 1; i >= 0; --i) {
            Tac cnt = now.ins.get(i);
            liveOut.put(cnt, (BitSet)now.in.clone());
            for (int k = 0; k < cnt.def().size(); ++k) {
                set(now.in, cnt.def().get(k), false);
            }
            for (int k = 0; k < cnt.ref().size(); ++k) {
                set(now.in, cnt.ref().get(k), true);
            }
            liveIn.put(cnt, (BitSet)now.in.clone());
        }
    }
    void LinearScan() {
        int[] first = new int[totId];
        int[] last = new int[totId];
        for (int i = 0; i < totId; ++i) {
            first[i] = last[i] = -1;
        }
        cntPos = totIns;
        for (int i = 0; i < bblocks.size(); ++i) {
            if (bblocks.get(i).isStart && !bblocks.get(i).visited) {
                dfs(bblocks.get(i));
            }
        }
        for (int i = 0; i < totIns; ++i) {
            BitSet now = status[i]; 
            for (int k = 0; k < totId; ++k) {
                if (now.get(k)) {
                    if (first[k] == -1 || i < first[k]) {
                        first[k] = i;
                    }
                    if (last[k] == -1 || i > last[k]) {
                        last[k] = i;
                    }
                }
            }
        }
        ArrayList<Interval> intervals = new ArrayList<Interval>();
        for (int i = 0; i < totId; ++i) {
            if (first[i] != -1) {
                intervals.add(new Interval(i, first[i], last[i])); 
            }
        }
        for (int i = 0; i < intervals.size(); ++i) {
            for (int j = i + 1; j < intervals.size(); ++j) {
                if (intervals.get(j).start < intervals.get(i).start) {
                    Object tmp = intervals.get(i);
                    intervals.set(i, intervals.get(j));
                    intervals.set(j, (Interval)tmp);
                }
            }
        }
        TreeSet<Interval> active = new TreeSet<Interval>();
        TreeSet<String> pool = new TreeSet<String>(Arrays.asList(new String[]{"$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7", "$t8", "$t9", "$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7", "$fp", "$k0", "$k1", "$v1", "$a3"}));
        remains = new TreeSet<String>(pool);
        for (int i = 0; i < intervals.size(); ++i) {
            Interval cnt = intervals.get(i);
            while (!active.isEmpty() && active.first().finish < cnt.start) {
                pool.add(active.first().reg);
                active.remove(active.first());
            }
            active.add(cnt);
            if (pool.isEmpty()) { 
                Interval spilled = active.last();
                active.remove(spilled);
                if (spilled != cnt) {
                    cnt.reg = spilled.reg;
                    spilled.reg = null;
                }
            }
            else {
                cnt.reg = pool.last();
                pool.remove(pool.last());
            }
        }
        for (int i = 0; i < intervals.size(); ++i) {
            Interval cnt = intervals.get(i);
            if (cnt.reg != null) {
                remains.remove(cnt.reg);
            }
            regs[cnt.id] = cnt.reg;
        }
    }
    void dfs(BBlock now) {
        now.visited = true;
        for (int i = 0; i < now.next.size(); ++i) {
            BBlock next = now.next.get(i);
            if (!next.visited) {
                dfs(now.next.get(i));
            }
        }
        for (int i = now.insNum - 1; i >= 0; --i) {
            status[--cntPos] = liveIn.get(now.ins.get(i));
        }
    }
}
