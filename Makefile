all:
	mkdir -p bin
	cd src/compiler/syntactic && make
	javac src/compiler/*/*.java -classpath lib/*.jar -d bin
clean:
	cd src/compiler/syntactic && make clean
	rm -rf bin
