la $a1, -32768($gp)

printf_loop: 
lb $a0, 0($a2) 
beq $a0, 0, printf_end
addu $a2, $a2, 1
beq $a0, '%', printf_fmt
li $v0, 11 
syscall
b printf_loop 

printf_fmt:
lb $a0, 0($a2)
addu $a2, $a2, 1
beq $a0, 'd', printf_int
beq $a0, 's', printf_str
beq $a0, 'c', printf_char
beq $a0, '0', printf_width

printf_int: 
addu $a1, $a1, 4
lw $a0, 0($a1)
li $v0, 1
syscall
b printf_loop 

printf_str: 
addu $a1, $a1, 4
lw $a0, 0($a1)
li $v0, 4
syscall
b printf_loop 

printf_char: 
addu $a1, $a1, 4
lw $a0, 0($a1)
li $v0, 11
syscall
b printf_loop

printf_width:
addu $a2, $a2, 2
lw $a0, 4($a1)
blt $a0, 10, printf_three_zeros
blt $a0, 100, printf_two_zeros
blt $a0, 1000, printf_one_zero
b printf_int

printf_three_zeros:
li $a0, 0
li $v0, 1
syscall
syscall
syscall
b printf_int

printf_two_zeros:
li $a0, 0
li $v0, 1
syscall
syscall
b printf_int

printf_one_zero:
li $a0, 0
li $v0, 1
syscall
b printf_int

printf_end:
